package ObserverPattern.second;

public interface DisplayElement {
    void display();
}
