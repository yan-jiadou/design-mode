package ObserverPattern.second;

public interface Observer {

    /**
     * 接收通知的方法
     * 发布者通过调用它来通知具体的观察者相关信息
     * @param o
     */
    void update(Object o);

}
