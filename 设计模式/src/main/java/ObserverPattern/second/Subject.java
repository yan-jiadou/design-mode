package ObserverPattern.second;

/**
 * 主题(发布者) 接口定义
 */
public interface Subject {

    /**
     * 观察者(订阅者) 注册方法
     * 观察者可以通过它实施对主题的订阅
     * @param observer
     */
    void registerObserver(Observer observer);

    /**
     * 取消订阅操作
     * @param observer
     */
    void removeObserver(Observer observer);

    /**
     * 通知方法
     * 主题通过它实现对订阅者的通知
     */
    void notifyObservers();


}
