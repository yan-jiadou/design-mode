package ObserverPattern.second;

/**
 * 气象站模拟
 */
public class WeatherStation {

    private WeatherData weatherData;

    public WeatherStation(WeatherData weatherData){
        this.weatherData = weatherData;
    }

    public void start(){
        //快速创建线程的方式
        new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep((long) (10000 * Math.random()));
                    weatherData.setHumidity((float) (100 * Math.random()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(()->{
            while (true){
                try {
                    Thread.currentThread().sleep((long)(10000*Math.random()));
                    weatherData.setTemperature((float) (40*Math.random()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();

        new Thread(()->{
            while (true) {
                try {
                    Thread.currentThread().sleep((long) (10000 * Math.random()));
                    weatherData.setPressure((float) (100 * Math.random()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
