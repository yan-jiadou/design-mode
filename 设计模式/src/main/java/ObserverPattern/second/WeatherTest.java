package ObserverPattern.second;



public class WeatherTest {
    public static void main(String[] args) {

        //新建weatherData并与气象站绑定
        Subject weatherData = new WeatherData();
        new WeatherStation((WeatherData) weatherData).start();

        // 新建观察者对象并订阅到指定的主题
        CurrentConditionDisplay currentConditionDisplay = new CurrentConditionDisplay(weatherData);

    }
}
