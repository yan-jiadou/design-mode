package ObserverPattern.second;



/**
 * 最新天气布告板，观察者(订阅者)的实现
 */
public class CurrentConditionDisplay implements Observer, DisplayElement {

    //天气数据
    private Subject weatherData;
    private float temp;
    private float humidity;

    public CurrentConditionDisplay(Subject weatherData){
        this.weatherData = weatherData;
        this.weatherData.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("当前温度："+temp+"\n"
        +"当前湿度："+humidity+"\n");
    }

    @Override
    public void update(Object object) {
        WeatherData weatherData = (WeatherData) object;
        temp = weatherData.getTemperature();
        humidity = weatherData.getHumidity();
        display();
    }


}
