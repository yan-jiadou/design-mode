package ObserverPattern.application;

import ObserverPattern.base.Subject;

public class WeatherData {
    Subject weatherSubject;
    //温度
    private float temperature;
    //湿度
    private float humidity;
    //压强
    private float pressure;

    public Subject getWeatherSubject() {
        return weatherSubject;
    }

    public void setWeatherSubject(Subject weatherSubject) {
        this.weatherSubject = weatherSubject;
    }

    public WeatherData() {
    }

    public void setMeasurements(float temperature,float humidity,float pressure){
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        weatherSubject.notifyObservers();
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
        weatherSubject.notifyObservers();
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
        weatherSubject.notifyObservers();
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
        weatherSubject.notifyObservers();
    }
}
