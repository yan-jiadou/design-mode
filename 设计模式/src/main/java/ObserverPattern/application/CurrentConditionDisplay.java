package ObserverPattern.application;

import ObserverPattern.base.DisplayElement;
import ObserverPattern.base.Observer;
import ObserverPattern.base.Subject;

/**
 * 最新天气布告板
 */
public class CurrentConditionDisplay implements Observer, DisplayElement {
    //天气主题
    private WeatherSubject weatherSubject;
    //天气数据
    private WeatherData weatherData;

    public CurrentConditionDisplay(WeatherSubject weatherSubject,WeatherData weatherData){
        this.weatherSubject = weatherSubject;
        this.weatherData = weatherData;
        this.weatherData.setWeatherSubject(weatherSubject);
        this.weatherSubject.setWeatherData(weatherData);
        this.weatherSubject.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("当前温度："+weatherData.getTemperature()+"\n"
        +"当前湿度："+weatherData.getHumidity()+"\n"+
                "当前压强:"+weatherData.getPressure()+"\n");
    }

    @Override
    public void update(Object object) {
        this.weatherData = (WeatherData) object;
        display();

    }

    public Subject getWeatherSubject() {
        return weatherSubject;
    }

    public void setWeatherSubject(WeatherSubject weatherSubject) {
        this.weatherSubject = weatherSubject;
    }

    public WeatherData getWeatherData() {
        return weatherData;
    }

    public void setWeatherData(WeatherData weatherData) {
        this.weatherData = weatherData;
    }
}
