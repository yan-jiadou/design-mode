package ObserverPattern.jdkApplication.event;

import java.util.EventObject;

/**
 * 事件类
 */
public class DoorEvent extends EventObject {

    private boolean doorState = false;
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public DoorEvent(Object source,boolean doorState) {
        super(source);
        this.doorState = doorState;
    }

    public void setDoorState(boolean doorState) {
        this.doorState = doorState;
    }

    public boolean getDoorState(){
        return this.doorState;
    }
}
