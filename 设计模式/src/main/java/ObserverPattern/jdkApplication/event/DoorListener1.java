package ObserverPattern.jdkApplication.event;

public class DoorListener1 implements DoorListener {

    @Override
    public void doorEvent(DoorEvent event) {
        if(event.getDoorState()){
            System.out.println("门1打开");
        }
        else{
            System.out.println("门1关闭");
        }
    }
}
