package ObserverPattern.jdkApplication.observable;

import java.util.Observable;

public class Publisher extends Observable {

    // 数字
    private int num;

    /**
     * 通知观察者们
     */
    public void notifyObservers() {
        super.notifyObservers(num);
    }

    public void setNum(int num) {
        this.num = num;
        this.setChanged();
        notifyObservers();
    }
}
