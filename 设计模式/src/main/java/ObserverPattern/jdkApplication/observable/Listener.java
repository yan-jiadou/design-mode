package ObserverPattern.jdkApplication.observable;

import java.util.Observable;
import java.util.Observer;

public class Listener implements Observer {


    @Override
    public void update(Observable o, Object arg) {
        System.out.println("观察到变化： "+arg);
    }

}
