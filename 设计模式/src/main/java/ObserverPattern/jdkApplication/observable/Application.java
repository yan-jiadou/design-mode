package ObserverPattern.jdkApplication.observable;

public class Application {

    public static void main(String[] args) {

        Publisher observable = new Publisher();

        observable.addObserver(new Listener());
        observable.addObserver(new Listener());

        observable.setNum(100);
        observable.setNum(1000);

    }
}
