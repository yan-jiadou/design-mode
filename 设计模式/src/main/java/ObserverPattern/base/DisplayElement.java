package ObserverPattern.base;

public interface DisplayElement {
    void display();
}
