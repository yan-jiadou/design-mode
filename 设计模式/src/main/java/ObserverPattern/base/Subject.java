package ObserverPattern.base;

/**
 * 主题接口,构造出一个主题该有的特征
 */
public interface Subject {
    /**
     * 观察者注册接口，此接口供观察者将订阅该主题
     * @param observer 观察者
     */
    void registerObserver(Observer observer);

    /**
     * 观察者注销接口，此接口供观察者取消订阅
     * @param observer 观察者
     */
    void removeObserver(Observer observer);

    /**
     * 通知接口，此接口供主题上级服务使用，
     * 当主题中的参数发生变化时，此方法将被调用以通知观察者
     */
    void notifyObservers();

}
