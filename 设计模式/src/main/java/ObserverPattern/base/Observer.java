package ObserverPattern.base;

public interface Observer {
    void update(Object object);
}
