package ObserverPattern.first;

/**
 * 模拟气象站
 */
public class WeatherStation {

    /**
     * 天气数据对象
     */
    private WeatherData weatherData;

    public WeatherStation(WeatherData weatherData){
        this.weatherData = weatherData;
    }

    public void start(){
        //快速创建线程的方式
        new Thread(() -> {
            while (true) {
                try {
                    Thread.currentThread().sleep((long) (10000 * Math.random()));
                    weatherData.setHumidity((float) (100 * Math.random()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(()->{
            while (true){
                try {
                    Thread.currentThread().sleep((long)(10000*Math.random()));
                    weatherData.setTemperature((float) (40*Math.random()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();

        new Thread(()->{
            while (true) {
                try {
                    Thread.currentThread().sleep((long) (10000 * Math.random()));
                    weatherData.setPressure((float) (100 * Math.random()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
