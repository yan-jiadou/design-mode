package ObserverPattern.first;

import java.util.ArrayList;
import java.util.List;

/**
 * 天气数据对象，可以被称为主题
 */
public class WeatherData {

    //温度
    private float temperature;
    //湿度
    private float humidity;
    //压强
    private float pressure;

    List<Display> displayList;

    public WeatherData() {
        displayList = new ArrayList<>();
    }

    /**
     * 添加布告板对象给WeatherData
     * @param display
     */
    public int addDisplay(Display display){
        displayList.add(display);
        return displayList.size()-1;
    }

    /**
     * 删除指定订阅的布告板
     * @param display
     */
    public void removeDisplay(int display){
        displayList.remove(display);
    }

    public void setMeasurements(float temperature,float humidity,float pressure){
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
        measurementsChanged();
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
        measurementsChanged();
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
        measurementsChanged();
    }

    /**
     * 此方法嵌入WeatherData中的set方法中，
     * 只要数据发生改变，就会调用此方法
     */
    public void measurementsChanged(){
        for(Display display:displayList){
            display.update(getTemperature(),getHumidity(),getPressure());
        }
    }

    @Override
    public String toString() {
        return "WeatherData{" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", pressure=" + pressure +
                '}';
    }
}
