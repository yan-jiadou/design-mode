package ObserverPattern.first;

/**
 * 天气预报预告版
 */
public class ForecastDisplay implements Display{

    private float temp;
    private float humidity;
    private  float pressure;

    public void update(float temp,float humidity,float pressure){
        this.temp=temp;
        this.humidity = humidity;
        this.pressure = pressure;
        display();
    }
    public void display(){
        System.out.println("天气预报布告板：温度："+temp+" 湿度："+humidity+" 压强："+pressure);
    }
}
