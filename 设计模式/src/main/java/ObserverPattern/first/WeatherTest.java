package ObserverPattern.first;

/**
 * 观察者模式也被称为： 发布-订阅模式
 *
 */
public class WeatherTest {

    public static void main(String[] args) throws InterruptedException {

        //新建weatherData并与气象站绑定
        WeatherData weatherData = new WeatherData();
        new WeatherStation(weatherData).start();

        System.out.println("天气数据： "+weatherData.toString());
        System.out.println("目前观察者注册数量："+weatherData.displayList.size());
        Thread.sleep(10000);

        //加入布告板
        int currentDis = weatherData.addDisplay(new CurrentConditionsDisplay());
        System.out.println("目前观察者注册数量："+weatherData.displayList.size());
        Thread.sleep(10000);
        int foreDis= weatherData.addDisplay(new ForecastDisplay());
        System.out.println("目前观察者注册数量："+weatherData.displayList.size());
        Thread.sleep(10000);
        int staticDis =  weatherData.addDisplay(new StatisticsDisplay());
        System.out.println("目前观察者注册数量："+weatherData.displayList.size());

    }
}
