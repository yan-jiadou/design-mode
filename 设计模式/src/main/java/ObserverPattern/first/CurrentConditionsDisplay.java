package ObserverPattern.first;

/**
 * 目前状况布告板, 它是订阅者
 */
public class CurrentConditionsDisplay implements Display{

    private float temp;
    private float humidity;
    private  float pressure;

    public void update(float temp,float humidity,float pressure){
        this.temp=temp;
        this.humidity = humidity;
        this.pressure = pressure;
        display();
    }

    public void display(){
        System.out.println("目前状况布告板：温度："+temp+" 湿度："+humidity);
    }
}
