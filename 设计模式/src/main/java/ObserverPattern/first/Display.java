package ObserverPattern.first;

/**
 * 观察者(也称订阅者)
 */
public interface Display {
    void update(float temp,float humidity,float pressure);
}
