package ObserverPattern;

import ObserverPattern.application.CurrentConditionDisplay;
import ObserverPattern.application.WeatherData;
import ObserverPattern.application.WeatherSubject;

/**
 * 观察者模式测试：
 * 观察者模式定义了对象之间的一对多依赖，这样一来，当一个对象改变状态时，
 * 它的所有依赖都会收到通知并自动更新。
 */
public class DisplayTest {
    public static void main(String[] args) {
        WeatherSubject weatherSubject = new WeatherSubject();
        WeatherData weatherData = new WeatherData();
        new CurrentConditionDisplay(weatherSubject,weatherData);
        weatherData.setMeasurements(10.0f,23.8f,23.7f);
        weatherData.setMeasurements(11.0f,3.8f,2.7f);
    }
}
