package AOP.application;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class Teacher implements InvocationHandler {

    private Object person;
    public int teacherId;

    public Teacher(){}
    public Teacher(String name,int age,String sex){
        person = new Person(name,age,sex);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        method.invoke(person,args);
        return null;
    }
}
