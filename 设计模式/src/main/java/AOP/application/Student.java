package AOP.application;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class Student implements InvocationHandler{

    private Object person;
    public int studyId;

    public Student(){}

    public Student(Person person){
      this.person = person;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        method.invoke(person, args);
        return null;
    }

}
