package AOP.application;

import java.lang.reflect.Proxy;

public class Test {
    public static void main(String[] args) {

        Person person = new Person("牧之",23,"男");

        Student student = new Student(person);

        personInterface proxyInstance  = (personInterface) Proxy.newProxyInstance(person.getClass().getClassLoader(), person.getClass().getInterfaces(),student);

        proxyInstance.disPlay();

        proxyInstance.speak();


    }
}
