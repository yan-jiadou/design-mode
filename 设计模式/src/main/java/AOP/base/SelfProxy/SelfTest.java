package AOP.base.SelfProxy;

import AOP.base.Hello;

import java.lang.reflect.InvocationTargetException;

public class SelfTest {
    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Hello hello = (Hello)new ProxySourceMaker(SelfHandler.class,Hello.class).getProxy();
        hello.sayHello();
    }
}
