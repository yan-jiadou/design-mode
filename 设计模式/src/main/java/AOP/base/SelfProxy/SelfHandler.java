package AOP.base.SelfProxy;

public class SelfHandler {

    public void before(){
        System.out.println("前置处理");
    }

    public void after(){
        System.out.println("后置处理");
    }

    public void around(){
        System.out.println("环绕处理");
    }

}
