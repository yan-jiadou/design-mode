package AOP.base;

public interface HelloInterface {
    Integer sayHello();
    void sayBye();
    void sayHei();
    void jojo();
}
