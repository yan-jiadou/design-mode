package AOP.base;

public class Hello implements HelloInterface{

    @Override
    public Integer sayHello() {
        System.out.println("hello world!!");
        return 8;
    }

    @Override
    public void sayBye() {
        System.out.println("BBye");
    }

    @Override
    public void sayHei() {

    }

    @Override
    public void jojo() {

    }
}
