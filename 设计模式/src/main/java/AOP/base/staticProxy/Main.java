package AOP.base.staticProxy;

import AOP.base.Hello;

public class Main {
    public static void main(String[] args) {
        new HelloProxy(new Hello()).sayHello();
    }
}
