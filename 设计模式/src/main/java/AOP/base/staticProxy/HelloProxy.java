package AOP.base.staticProxy;

import AOP.base.HelloInterface;

public class HelloProxy implements HelloInterface {

    private final HelloInterface hello;

    @Override
    public Integer sayHello() {
        System.out.println("my name is wang");
        hello.sayHello();
        return 8;
    }

    @Override
    public void sayBye() {
        System.out.println("拜拜");
    }

    @Override
    public void sayHei() {

    }

    @Override
    public void jojo() {

    }

    public HelloProxy(HelloInterface hello){
        this.hello = hello;
    }
}
