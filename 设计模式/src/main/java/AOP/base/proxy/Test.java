package AOP.base.proxy;

import AOP.base.Hello;
import AOP.base.HelloInterface;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Test {
    public static void main(String[] args) {

        System.getProperties().setProperty("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        HelloInterface hello = new Hello();

        InvocationHandler handler = new ProxyHandler(hello);

       //Class[] classes = hello.getClass().getInterfaces();

        //执行被代理对象的相关逻辑
        HelloInterface proxyHello = (HelloInterface) Proxy.newProxyInstance(hello.getClass().getClassLoader(), hello.getClass().getInterfaces(), handler);

        proxyHello.sayHello();

        proxyHello.sayBye();

        ProxyUtil.generateClassFile(hello.getClass(), "UserServiceProxy");

    }
}
