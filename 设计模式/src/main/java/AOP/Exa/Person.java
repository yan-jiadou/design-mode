package AOP.Exa;

public class Person {
    public String name;
    public int age;
    public String sex;
    public Person(){}
    public Person(String name,int age,String sex){
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
    public void disPlay() {
        System.out.println("我的名字是："+name);
    }
    public void speak(){
        System.out.println("你好！！");
    }
}
