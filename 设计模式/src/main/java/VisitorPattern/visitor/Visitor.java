package VisitorPattern.visitor;

import VisitorPattern.shapes.Circle;
import VisitorPattern.shapes.CompoundShape;
import VisitorPattern.shapes.Dot;
import VisitorPattern.shapes.Rectangle;

public interface Visitor {
    String visitDot(Dot dot);

    String visitCircle(Circle circle);

    String visitRectangle(Rectangle rectangle);

    String visitCompoundGraphic(CompoundShape cg);
}
