package BuilderPattern.second.builders;

import BuilderPattern.second.cars.CarType;
import BuilderPattern.second.components.Engine;
import BuilderPattern.second.components.GPSNavigator;
import BuilderPattern.second.components.Transmission;
import BuilderPattern.second.components.TripComputer;

public interface Builder {
    void setCarType(CarType type);
    void setSeats(int seats);
    void setEngine(Engine engine);
    void setTransmission(Transmission transmission);
    void setTripComputer(TripComputer tripComputer);
    void setGPSNavigator(GPSNavigator gpsNavigator);
}
