package BuilderPattern.first;

public class House {
    //地板
    private String floor;
    //墙
    private String wall;
    //门
    private String door;
    //窗户
    private String window;
    //屋顶
    private String roof;

    public House(String floor,String wall,String door,String window,String roof){
        this.floor = floor;
        this.wall = wall;
        this.door = door;
        this.window = window;
        this.roof = roof;
    }

    public String toString(){
        System.out.println("恭喜您，建造了一个"+floor+wall+door+window+roof+" 的房子");
        return "恭喜您，建造了一个"+floor+wall+door+window+roof+" 的房子";
    }

}
