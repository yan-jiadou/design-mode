package BuilderPattern.first;

public class HouseBuilder {
    House house;
    //地板
    private String floor;
    //墙
    private String wall;
    //门
    private String door;
    //窗户
    private String window;
    //屋顶
    private String roof;

    public HouseBuilder buildWalls(String wall){
        this.wall = wall;
        return this;
    }

    public HouseBuilder buildDoors(String door){
        this.door = door;
        return this;
    }

    public HouseBuilder buildWindows(String window){
        this.window = window;
        return this;
    }

    public HouseBuilder buildRoof(String roof){
        this.roof = roof;
        return this;
    }

    public HouseBuilder buildFloor(String floor){
        this.floor = floor;
        return this;
    }

    public House getResult(){
        return new House(floor,wall,door,window,roof);
    }

}
