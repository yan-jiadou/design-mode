package BuilderPattern.first;

public class Demo {
    public static void main(String[] args) {

        System.out.println("普通构造函数创建：");
        House house = new House("木质地板","白色墙面","钢门","木窗","平顶");
        house.toString();

        System.out.println("建造器进行构造： ");
        HouseBuilder houseBuilder = new HouseBuilder();
        houseBuilder.buildDoors("木制的门");
        houseBuilder.buildRoof("金色的屋顶");
        House house1 = houseBuilder.getResult();
        house1.toString();
        System.out.println("流式建造器进行构造： ");
        HouseBuilder builder = new HouseBuilder();
        builder.buildRoof("黄色屋顶").buildFloor("黑色地板");
        builder.getResult().toString();
    }
}
