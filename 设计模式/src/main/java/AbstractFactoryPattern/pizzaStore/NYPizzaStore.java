package AbstractFactoryPattern.pizzaStore;

import AbstractFactoryPattern.base.Pizza;
import AbstractFactoryPattern.base.PizzaFactory;
import AbstractFactoryPattern.base.PizzaStore;
import AbstractFactoryPattern.factory.NYPizzaFactory;


/**
 * NY披萨分店
 */
public class NYPizzaStore extends PizzaStore {

    PizzaFactory pizzaFactory;

    public NYPizzaStore(){
        this.pizzaFactory = new NYPizzaFactory();
    }

    public NYPizzaStore(PizzaFactory pizzaFactory){
        this.pizzaFactory = pizzaFactory;
    }

    @Override
    protected Pizza createPizza(String type) {
        return pizzaFactory.createPizza(type);
    }
}
