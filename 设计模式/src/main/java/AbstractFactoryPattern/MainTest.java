package AbstractFactoryPattern;

import AbstractFactoryPattern.base.PizzaStore;
import AbstractFactoryPattern.factory.NYPizzaFactory;
import AbstractFactoryPattern.pizzaStore.NYPizzaStore;

/**
 * 抽象工厂模式提供一个接口，用于创建相关或依赖对象的家族，而不需要明确指定具体类。 这种方式依赖注入
 */
public class MainTest {
    public static void main(String[] args) {
        PizzaStore nyPizzaStore = new NYPizzaStore();
        nyPizzaStore.orderPizza("cheese");

        PizzaStore pizzaStore = new NYPizzaStore(new NYPizzaFactory());
        pizzaStore.orderPizza("cheese");
    }
}
