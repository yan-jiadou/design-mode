package AbstractFactoryPattern.base;

public interface PizzaIngredientFactory {
    String createDough();
    String createSauce();
    String createCheese();
    String[] createVeggies();
    String createPepperoni();
    String createClam();
}
