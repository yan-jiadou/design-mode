package AbstractFactoryPattern.base;

import java.util.Arrays;

public abstract class Pizza {
    public String name;
//    public String dough;
//    public String sauce;
//    public String[] veggies;
//    public String cheese;
//    public String pepperoni;
//    public String clam;


//    @Override
//    public String toString() {
//        return "Pizza{" +
//                "name='" + name + '\'' +
//                ", dough='" + dough + '\'' +
//                ", sauce='" + sauce + '\'' +
//                ", veggies=" + Arrays.toString(veggies) +
//                ", cheese='" + cheese + '\'' +
//                ", pepperoni='" + pepperoni + '\'' +
//                ", clam='" + clam + '\'' +
//                '}';
//    }

    public abstract void prepare();

    protected void bake(){
        System.out.println("准备烘烤 25 分钟...");
    }

    protected void cut(){
        System.out.println("烘烤完成，进行切割");
    }

    protected void box(){
        System.out.println("切割完毕，进行装盒");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
