package AbstractFactoryPattern.base;

public interface PizzaFactory {
    Pizza createPizza(String type);
}
