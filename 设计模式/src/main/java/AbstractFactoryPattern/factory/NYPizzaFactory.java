package AbstractFactoryPattern.factory;
import AbstractFactoryPattern.base.Pizza;
import AbstractFactoryPattern.base.PizzaFactory;
import AbstractFactoryPattern.pizza.CheesePizza;
import AbstractFactoryPattern.pizza.ClamPizza;

/**
 * Pizza工厂
 */
public class NYPizzaFactory implements PizzaFactory {
    @Override
    public Pizza createPizza(String type) {
        Pizza pizza = null;
        if(type.equals("cheese")){
            pizza = new CheesePizza();
            pizza.setName("New York Style Cheese Pizza");
        }else if(type.equals("clam")){
            pizza = new ClamPizza();
            pizza.setName("New York Style Clam Pizza");
        }
        return pizza;
    }
}
