package AbstractFactoryPattern.factory;

import AbstractFactoryPattern.base.PizzaIngredientFactory;

/**
 * 原料工厂
 */
public class NYPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public String createDough() {
        return "Dough";
    }

    @Override
    public String createSauce() {
        return "Sauce";
    }

    @Override
    public String createCheese() {
        return "Cheese";
    }

    @Override
    public String[] createVeggies() {
        return new String[] {"Veggies1","Veggies2"};
    }

    @Override
    public String createPepperoni() {
        return "Pepperoni";
    }

    @Override
    public String createClam() {
        return "Clam";
    }
}
