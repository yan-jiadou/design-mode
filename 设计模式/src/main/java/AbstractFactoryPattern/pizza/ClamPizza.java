package AbstractFactoryPattern.pizza;

import AbstractFactoryPattern.base.Pizza;
import AbstractFactoryPattern.base.PizzaIngredientFactory;

public class ClamPizza extends Pizza {
    PizzaIngredientFactory ingredientFactory;

    public ClamPizza(PizzaIngredientFactory ingredientFactory){
        this.ingredientFactory = ingredientFactory;
    }
    public ClamPizza(){

    }

    @Override
    public void prepare() {
        System.out.println("Preparing "+name);
//        dough = ingredientFactory.createDough();
//        sauce = ingredientFactory.createSauce();
//        cheese = ingredientFactory.createCheese();
    }

    @Override
    protected void bake(){
        System.out.println("ClamPizza烘烤方式------");
    }
}
