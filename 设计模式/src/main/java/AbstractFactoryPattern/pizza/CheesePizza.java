package AbstractFactoryPattern.pizza;

import AbstractFactoryPattern.base.Pizza;
import AbstractFactoryPattern.base.PizzaIngredientFactory;

/**
 * Cheese披萨定义类
 * 抽象工厂模式提供一个接口，用于创建相关或依赖对象的家族，而不需要明确指定具体类。
 */
public class CheesePizza extends Pizza {

    PizzaIngredientFactory ingredientFactory;

    public CheesePizza(){

    }

    public CheesePizza(PizzaIngredientFactory ingredientFactory){
        this.ingredientFactory = ingredientFactory;
    }

    @Override
    public void prepare() {
        System.out.println("Preparing "+name);
//        dough = ingredientFactory.createDough();
//        sauce = ingredientFactory.createSauce();
//        cheese = ingredientFactory.createCheese();
    }

    @Override
    protected void bake(){
        System.out.println("Cheese烘烤方式：。。。，，，。。。");
    }

}
