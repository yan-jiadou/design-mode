package FlyweightPattern.betterFirst;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class FireWorksThread extends Thread {
    private static JPanel panel;
    Graphics g;
    // 保存粒子的队列
    private final ArrayList<Particle> particleArrayList = new ArrayList<>();
    // 起始位置
    private int startX = 500, startY = 500;
    private final List<Color> colorList = new ArrayList<>();
    private final Color c = new Color(255,0,0);

    public FireWorksThread(JPanel panel) {
        FireWorksThread.panel = panel;
        for(int i=0;i<10;i++){
            int r = c.getRGB();
            colorList.add(new Color((int) (r-(int)1000*(Math.random()))));
        }
    }

    public void setStartXY(int x, int y) {
        this.startX = x;
        this.startY = y;
    }

    public void run() {
        // 时间增量
        double dt = 0.1d;
        while (true) {
            // 画背景
            g = panel.getGraphics();
            // 生成粒子放入链表
            Particle tp = new Particle();
            tp.position = new VecT(startX, startY);
            tp.velocity = new VecT(10, -20);// 速度向量
            tp.acceleration = sampleDirection();
            tp.life = 30;
            tp.age = 1;
            tp.color = colorList.get((int) (Math.random() * 10));
            tp.size = 12;
            particleArrayList.add(tp);

            // 链表中的粒子画到缓冲区，再画到界面上
            Image image = panel.createImage(panel.getWidth(), panel.getHeight());
            Graphics2D bg = (Graphics2D) image.getGraphics();

            bg.setColor(Color.black);
            bg.fillRect(0, 0, panel.getWidth(), panel.getHeight());// 画背景
            bg.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            //System.out.println(particleArrayList.size());
            for (Particle p : particleArrayList) {
                // 判断是否碰撞到墙
                fireBound(p);
                p.position = p.position.add(p.velocity.multiply(dt));
                p.velocity = p.velocity.add(p.acceleration.multiply(dt));
                // 画到缓冲区
                bg.setColor(p.color);
                bg.fillOval(p.getX(), p.getY(), p.size, p.size);
            }
            // 将缓冲图片画到面板上
            g.drawImage(image, 0, 0, null);
        }
    }

    // 让粒子碰到墙后反弹
    public static void fireBound(Particle p) {
        if (p.getX() <= 0 || p.getX() >= panel.getWidth())
            p.velocity.x *= -1;
        if (p.getY() <= 0 || p.getY()>=panel.getHeight())
            p.velocity.y *= -1;
    }
    // 生成一个随机方向
    public static VecT sampleDirection() {
        double theta = Math.random() * 2 * Math.PI;
        return new VecT((Math.cos(theta)), (Math.sin(theta)));
    }
}
