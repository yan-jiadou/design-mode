package FlyweightPattern.betterFirst;

import javax.swing.*;

public class FireWorksMain extends JPanel {

    public static void main(String[] args) {
       new FireWorksMain().initUI();
    }

    public void initUI() {
        JFrame f = new JFrame();
        f.setTitle("粒子系统");
        f.setSize(800, 700);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.add(this);
        f.setVisible(true);//初始化窗口界面
        FireWorksThread fire = new FireWorksThread(this);
        fire.start();//开启线程
        FwListener fl = new FwListener(fire);
        this.addMouseMotionListener(fl);//鼠标监听事件
    }
}
