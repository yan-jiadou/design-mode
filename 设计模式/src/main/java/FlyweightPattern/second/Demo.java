package FlyweightPattern.second;

import FlyweightPattern.second.forest.Forest;

import javax.swing.*;
import java.awt.*;

/**
 * 共享模式模式主要用来减少创建 存储重复数据的对象 而造成的无效内存占用
 */
public class Demo {
    //画板大小
    static int CANVAS_SIZE = 500;
    //需要画的tree的数量
    static int TREES_TO_DRAW = 1000000;
    //tree类型数量
    static int TREE_TYPES = 2;

    public static void main(String[] args) {

        //创建Forest对象
        Forest forest = new Forest();

        for (int i = 0; i < Math.floor(TREES_TO_DRAW / TREE_TYPES); i++) {
            forest.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Summer Oak", Color.GREEN, "Oak texture stub");
            forest.plantTree(random(0, CANVAS_SIZE), random(0, CANVAS_SIZE),
                    "Autumn Oak", Color.ORANGE, "Autumn Oak texture stub");
        }
        forest.setSize(CANVAS_SIZE, CANVAS_SIZE);
        forest.setVisible(true);
        forest.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        System.out.println(TREES_TO_DRAW + " trees drawn");
        System.out.println("---------------------");
        System.out.println("Memory usage:");
        System.out.println("Tree size (8 bytes) * " + TREES_TO_DRAW);
        System.out.println("+ TreeTypes size (~30 bytes) * " + TREE_TYPES + "");
        System.out.println("---------------------");
        System.out.println("Total: " + ((TREES_TO_DRAW * 8 + TREE_TYPES * 30) / 1024 / 1024) +
                "MB (instead of " + ((TREES_TO_DRAW * 38) / 1024 / 1024) + "MB)");
    }

    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}
