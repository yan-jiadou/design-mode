package FlyweightPattern.second.trees;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * TreeType工厂，
 * 将创建TreeType的细节聚集在这个类中
 */
public class TreeFactory {

    //缓存对象池,避免创建多个相同的对象
    static Map<String, TreeType> treeTypes = new HashMap<>();

    public static TreeType getTreeType(String name, Color color, String otherTreeData) {
        TreeType result = treeTypes.get(name);
        if (result == null) {
            result = new TreeType(name, color, otherTreeData);
            treeTypes.put(name, result);
        }
        return result;
    }

}
