package FlyweightPattern.second.trees;

import java.awt.*;

/**
 * Tree对象，用组合的方式使用亨元对象TreeType
 */
public class Tree {
    //树的x坐标
    private int x;
    //树的y坐标
    private int y;
    //树的TreeType属性
    private TreeType type;

    public Tree(int x, int y, TreeType type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public void draw(Graphics g) {
        type.draw(g, x, y);
    }
}
