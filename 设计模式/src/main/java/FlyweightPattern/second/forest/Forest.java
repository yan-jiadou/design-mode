package FlyweightPattern.second.forest;

import FlyweightPattern.second.trees.Tree;
import FlyweightPattern.second.trees.TreeFactory;
import FlyweightPattern.second.trees.TreeType;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Forest类，这个类用来绘制树
 */
public class Forest extends JFrame {

    //存储Tree对象的数组,Tree用来存储绘制树的数据
    private List<Tree> trees = new ArrayList<>();

    public void plantTree(int x, int y, String name, Color color, String otherTreeData) {
        TreeType type = TreeFactory.getTreeType(name, color, otherTreeData);
        Tree tree = new Tree(x, y, type);
        trees.add(tree);
    }

    @Override
    public void paint(Graphics graphics) {
        for (Tree tree : trees) {
            tree.draw(graphics);
        }
    }
}
