package FlyweightPattern.first;

public class Main {
    public static void main(String[] args) {
        new FireWorksMain().initUI();
        new Thread(()->{
            new FireWorksMain().initUI();
        }).start();
    }
}

