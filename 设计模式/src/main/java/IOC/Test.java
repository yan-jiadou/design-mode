package IOC;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Test {

    public static void main(String[] args) {
        new Test().IOCCoding();
        new Test().NormalCoding();
    }

    public void IOCCoding(){
        ObjectConfig objectConfig = new ObjectConfig();
        objectConfig.setBeanName("person");
        objectConfig.setCl(Person.class);
        Map<String ,Object> diMap = objectConfig.getDiMap();
        diMap.put("name","牧之");
        diMap.put("age",12);
        diMap.put("sex","男");
        diMap.put("height",170);
        diMap.put("weight",110);
        List<ObjectConfig> objectConfigList = new ArrayList<>();
        objectConfigList.add(objectConfig);

        ApplicationContext applicationContext = new ApplicationContext(objectConfigList);

        Person person = (Person) applicationContext.getObject("person");
        System.out.println(person.toString());
    }

    public void NormalCoding(){
        Person person = new Person();
        person.setName("四郎");
        person.setAge(12);
        person.setSex("男");
        person.setHeight(170);
        person.setWeight(110);
        System.out.println(person);
    }

}
