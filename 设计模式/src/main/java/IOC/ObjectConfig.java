package IOC;

import java.util.HashMap;
import java.util.Map;

public class ObjectConfig {

    /**
     * 存储对象名称
     */
    private String beanName;
    /**
     * 存储对象类型
     */
    private Class<?> cl;
    /**
     * 存储对象属性名称和赋的值
     */
    private Map<String,Object> diMap;
    /**
     * 标注注入的字段类型是基本类型还是引用类型
     */
    private Map<String,String> valueType;

    public ObjectConfig(){
        diMap = new HashMap<>();
        valueType = new HashMap<>();
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public Class<?> getCl() {
        return cl;
    }

    public void setCl(Class<?> cl) {
        this.cl = cl;
    }

    public Map<String, Object> getDiMap() {
        return diMap;
    }

    public void setDiMap(Map<String, Object> diMap) {
        this.diMap = diMap;
    }

    public Map<String, String> getValueType() {
        return valueType;
    }

    public void setValueType(Map<String, String> valueType) {
        this.valueType = valueType;
    }
}
