package CompositePattern.second;

/**
 * 组合模式允许你将对象组合成树形结构来表现 “整体/部分” 层次结构。
 * 组合能让客户以一致的方式处理个别对象及对象组合。
 */
public class MenuTestDrive {
    public static void main(String[] args) {
        //主菜单
        MenuComponent allMenus = new Menu("ALL MENUS","All menus combined");
        MenuComponent pancakeHouseMenu = new Menu("PANCAKE HOUSE MENU","Breakfast");
        pancakeHouseMenu.add(new MenuItem("土豆西红柿","就是土豆炒西红柿",true,4.77));
        MenuComponent dinerMenu = new Menu("DINER MENU","lunch");
        dinerMenu.add(new MenuItem("牛肉炒西红柿","西红柿炖牛肉",false,99.99));
        MenuComponent cafeMenu = new Menu("CAFE MENU","Dinner");
        MenuComponent dessertMenu = new Menu("DESSERT MENU","Dessert of course!");
        allMenus.add(pancakeHouseMenu);
        allMenus.add(dessertMenu);
        allMenus.add(dinerMenu);
        allMenus.add(cafeMenu);
        allMenus.print();
    }
}
