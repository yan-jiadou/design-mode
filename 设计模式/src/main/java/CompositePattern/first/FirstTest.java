package CompositePattern.first;

import java.lang.reflect.InvocationTargetException;

public class FirstTest {
    public static void main(String[] args) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        new Alice().printMenuByIterator();
    }
}
