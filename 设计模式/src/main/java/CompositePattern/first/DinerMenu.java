package CompositePattern.first;

import java.lang.reflect.InvocationTargetException;

/**
 * 夜晚餐厅菜单
 */
public class DinerMenu extends Menu{

    static final int MAX_ITEMS = 6;
    public int numberOfItems = 0;
    MenuItem[] menuItems;


    public DinerMenu() throws InstantiationException, IllegalAccessException, InvocationTargetException {
        menuItems = new MenuItem[MAX_ITEMS];

        addItem("Vegetarian BLT","(Fakin) Bacon with lettuce & tomato on whole wheat",true,2.99);

        addItem("BLT","Bacon with lettuce & tomato on whole wheat",false,2.99);

        addItem("Hotdog","A hot dog",false,0);

        menuItems[2].addSonMenu(DinerMenu.class);
        Menu sonMenu = menuItems[2].sonMenu;
        sonMenu.addItem("hotdog1","hotdog1",false,9.99);
        sonMenu.addItem("hotdog2","hotdog2",false,2.99);
        sonMenu.addItem("hotdog3","hotdog3",false,9.99);
    }

    public DinerMenu(boolean son){
        menuItems = new MenuItem[MAX_ITEMS];
    }

    public void addItem(String name,String description,boolean vegetarian,double price){
        MenuItem menuItem = new MenuItem(name,description,vegetarian,price);
        if(numberOfItems >= MAX_ITEMS) {
            System.out.println("Sorry, menu is null! Can't add item to menu");
        }else{
            menuItems[numberOfItems] = menuItem;
            numberOfItems = numberOfItems + 1;
        }
    }

    public MenuItem[] getMenuItems(){
        return menuItems;
    }

    public Iterator createIterator(){
        return new DinerMenuIterator(menuItems);
    }


    class DinerMenuIterator implements Iterator {
        MenuItem[] items;
        int position = 0;

        public DinerMenuIterator(MenuItem[] items){
            this.items = items;
        }

        @Override
        public boolean hasNext() {
            if(position>=items.length||items[position]==null){
                return false;
            }else {
                return true;
            }
        }

        @Override
        public Object next() {
            MenuItem menuItem = items[position];
            position+=1;
            return menuItem;
        }

    }

}
