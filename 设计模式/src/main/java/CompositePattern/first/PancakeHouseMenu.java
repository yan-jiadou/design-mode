package CompositePattern.first;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * 煎饼屋菜单
 */
public class PancakeHouseMenu extends Menu{

    ArrayList menuItems;

    public PancakeHouseMenu() throws InvocationTargetException, InstantiationException, IllegalAccessException {

       menuItems = new ArrayList();

       addItem("K&B's Pancake Breakfast","Pancakes with scrambled eggs,and toast",true,2.99);

       addItem("Regular Pancake Breakfast","Pancakes with fried eggs, sausage",false,3.49);

       addItem("Blueberry Pancakes","Pancakes made with fresh blueberries",true,2.99);

       addItem("Waffles","Waffles ,with your choice of blueberries or strawberries",true,3.59);

        MenuItem menuItem = (MenuItem)menuItems.get(2);
        menuItem.addSonMenu(PancakeHouseMenu.class);
        Menu sonMenu = menuItem.sonMenu;
        sonMenu.addItem("hotdog1","hotdog1",false,9.99);
        sonMenu.addItem("hotdog2","hotdog2",false,2.99);
        sonMenu.addItem("hotdog3","hotdog3",false,9.99);
    }

    public PancakeHouseMenu(boolean son){
        menuItems = new ArrayList();
    }

    public void addItem(String name, String description,boolean vegetarian,double price){
        MenuItem menuItem = new MenuItem(name,description,vegetarian,price);
        menuItems.add(menuItem);
    }

    public ArrayList getMenuItems(){
        return menuItems;
    }

    public Iterator createIterator(){
        return new PancakeHouseMenuIterator(menuItems);
    }

    class PancakeHouseMenuIterator implements Iterator {

        ArrayList menuItems;
        int position = 0;

        public PancakeHouseMenuIterator(ArrayList menuItems){
            this.menuItems = menuItems;
        }

        @Override
        public boolean hasNext() {
            if(position>=menuItems.size()||menuItems.get(position)==null)
                return false;
            else return true;
        }

        @Override
        public Object next() {
            MenuItem menuItem = (MenuItem) menuItems.get(position);
            position+=1;
            return menuItem;
        }
    }

}
