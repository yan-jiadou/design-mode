package CompositePattern.first;

public abstract class Menu {

    public abstract void addItem(String name, String description,boolean vegetarian,double price);

    public abstract Iterator createIterator();

}
