package CompositePattern.first;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;

/**
 * 餐点实体类
 */
public class MenuItem {
    //菜名
    String name;
    //菜的描述
    String description;
    //是否是素食
    boolean vegetarian;
    //价格
    double price;

    Menu sonMenu;

    public MenuItem(String name, String description, boolean vegetarian, double price){
        this.name = name;
        this.description = description;
        this.vegetarian = vegetarian;
        this.price = price;
    }

    public void addSonMenu(Class<?> cl) throws InstantiationException, IllegalAccessException, InvocationTargetException {
        Constructor<?>[] constructors = cl.getConstructors();
        for(Constructor<?> constructor:constructors){
            if(constructor.getParameters().length==1){
                Parameter[] parameters = constructor.getParameters();
                if(parameters[0].getType().isAssignableFrom(boolean.class)){
                    sonMenu = (Menu) constructor.newInstance(true);
                }
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
