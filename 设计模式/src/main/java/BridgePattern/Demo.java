package BridgePattern;

import BridgePattern.devices.Device;
import BridgePattern.devices.Radio;
import BridgePattern.devices.Tv;
import BridgePattern.remotes.AdvancedRemote;
import BridgePattern.remotes.BasicRemote;

/**
 * 桥接模式
 */
public class Demo {

        public static void main(String[] args) {
            testDevice(new Tv());
            testDevice(new Radio());
        }

        public static void testDevice(Device device) {
            System.out.println("Tests with basic remote.");
            BasicRemote basicRemote = new BasicRemote(device);
            basicRemote.power();
            device.printStatus();

            System.out.println("Tests with advanced remote.");
            AdvancedRemote advancedRemote = new AdvancedRemote(device);
            advancedRemote.power();
            advancedRemote.mute();
            device.printStatus();
        }

}
