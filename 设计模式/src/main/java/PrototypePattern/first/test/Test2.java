package PrototypePattern.first.test;

import PrototypePattern.first.model.Student;

public class Test2 {
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("黄四郎");
        student.setNum(1);
        student.setAge(40);
        student.setWeight(180);
        System.out.println(student.toString());

        Student student1 =  student.clone();
        System.out.println(student1);
    }
}
