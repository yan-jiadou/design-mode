package PrototypePattern.first.test;

import PrototypePattern.first.model.Student;

public class Test1 {
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("黄四郎");
        student.setNum(1);
        student.setAge(40);
        student.setWeight(180);
        System.out.println(student.toString());

        Student student1 = new Student();
        System.out.println(student1.toString());
        student1.setName(student.getName());
        student1.setNum(student.getNum());
        student1.setAge(student.getAge());
        student1.setWeight(student.getWeight());
        System.out.println(student1);

    }
}
