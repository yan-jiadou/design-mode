package PrototypePattern.first;

public interface Prototype {

    Prototype clone();
}
