package PrototypePattern.first.model;

import PrototypePattern.first.Prototype;

public class Student implements Prototype {

    private String name;
    private int num;
    private int age;
    private int weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", num=" + num +
                ", age=" + age +
                ", weight=" + weight +
                '}';
    }

    @Override
    public Student clone() {
        Student student = new Student();
        student.name = this.name;
        student.age = this.age;
        student.num = this.num;
        student.weight = this.weight;
        return student;
    }
}
