package StatePattern.second;

public class SoldState implements State{

    GumballMachine gumballMachine;

    public SoldState(GumballMachine gumballMachine){
        this.gumballMachine =gumballMachine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("稍等一下，正在为您准备糖果哦");

    }

    @Override
    public void ejectQuarter() {
        System.out.println("抱歉，你已经拉动了拉杆，不能退了哦");

    }

    @Override
    public void turnCrank() {
        System.out.println("你已经拉了一次了，不要重复拉哦");

    }

    @Override
    public void dispense() {
        gumballMachine.releaseBall();
        if(gumballMachine.count>0){
            gumballMachine.state = gumballMachine.noQuarterState;
        }else{
            System.out.println("糖果已经买完了");
            gumballMachine.state = gumballMachine.soldOutState;
        }

    }
}
