package StatePattern.second;


public class SoldOutState implements State {

    private GumballMachine gumballMachine;

    public SoldOutState(GumballMachine gumballMachine){
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("糖果已经卖完了哦，不能再投入硬币了");

    }

    @Override
    public void ejectQuarter() {
        System.out.println("糖果已经卖完了！！");

    }

    @Override
    public void turnCrank() {
        System.out.println("糖果已经卖完了，无效拉杆！！");

    }

    @Override
    public void dispense() {
        System.out.println("糖果已经卖完了，不能再吐出糖果了哦");

    }
}
