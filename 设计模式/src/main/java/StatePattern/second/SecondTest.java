package StatePattern.second;

/**
 *  状态模式允许对象在内部状态改变时改变它的行为，对象看起来好像修改了它的类
 */
public class SecondTest {
    public static void main(String[] args) {
        GumballMachine gumballMachine = new GumballMachine(3);

        gumballMachine.insertQuarter();
        gumballMachine.ejectQuarter();
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();
        gumballMachine.turnCrank();

    }
}
