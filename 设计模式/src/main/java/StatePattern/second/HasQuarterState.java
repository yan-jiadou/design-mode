package StatePattern.second;

public class HasQuarterState implements State{

    GumballMachine gumballMachine;

    public HasQuarterState(GumballMachine gumballMachine){
        this.gumballMachine = gumballMachine;
    }


    @Override
    public void insertQuarter() {
        System.out.println("你已经投了一个硬币了，不能再投了哦");

    }

    @Override
    public void ejectQuarter() {
        System.out.println("好的，退给你你的硬币呢");
        gumballMachine.state = gumballMachine.noQuarterState;
    }

    @Override
    public void turnCrank() {
        System.out.println("你拉动了拉杆....");
        gumballMachine.state = gumballMachine.soldState;

    }

    @Override
    public void dispense() {
        System.out.println("没有糖果吐出来");

    }
}
