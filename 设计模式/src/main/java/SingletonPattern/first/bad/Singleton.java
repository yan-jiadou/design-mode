package SingletonPattern.first.bad;

/**
 * 一个单例类的特点有： 构造器私有, 自引用类型私有, 只保留一个公开的获取类示例的入口
 *
 * 普通懒汉式单例类，应用场景：
 * 注册表设置，连接池，线程池等等。
 * 利用单例模式对象，我们可以确保程序中使用的全局资源只有一份
 */
public class Singleton {

    //自类型引用私有
    private static Singleton instance;

    //构造方法私有化
    private Singleton(){}

    /**
     * 公共访问点,
     * 设置这个唯一获取示例的入口,并在其中限制这个类只有一个示例
     * @return
     */
    public static Singleton getInstance() {
        if(instance==null){
            instance = new Singleton();
        }
        return instance;
    }

    public void sayHello(){
        System.out.println("Hello world!!");
    }
}
