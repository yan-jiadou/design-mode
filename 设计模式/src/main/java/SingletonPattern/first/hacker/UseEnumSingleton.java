package SingletonPattern.first.hacker;

import SingletonPattern.first.implway.EnumSingleton;

public class UseEnumSingleton {
    public static void main(String[] args) {
        EnumSingleton enumSingleton = EnumSingleton.INSTANCE;
        enumSingleton.setCode("code");
        enumSingleton.setName("枚举单例类");
        System.out.println( enumSingleton.getName());
        for(int i=0;i<1000;i++){
            new Thread(()->{
                System.out.println(EnumSingleton.INSTANCE.getCode());
            }).start();
        }
    }
}
