package SingletonPattern.first.implway;

/**
 * 饿汉式单例实现
 * 线程安全,因为在类加载的时候就已经创建实例了
 * 缺点是如果单例类很大并不常用到的话, 会出现内存浪费的情况
 */
public class HungrySingleton {

    private final static HungrySingleton hungrySingleton= new HungrySingleton();

    private HungrySingleton(){}

    public static HungrySingleton getInstance(){
        return hungrySingleton;
    }

}
