package SingletonPattern.first.implway;

/**
 * 枚举单例实现
 * 其实我们采用普通私有化构造器方法是实现单例类的方式在反射面前并不可靠
 * 因为一些客户端使用反射机制能够实例化单例类的第二个实例甚至第n个实例，
 * 如果我们想要抵御这样的攻击，我们可以人为增加限制，让构造器进行自我检查，就是在构造器尝试实例化第二个实例的时候抛出异常
 * 单例枚举单例可以不用那么麻烦就能做到抵御前面的攻击,枚举对象天然就是单例
 */
public enum EnumSingleton {

    INSTANCE("枚举单例类","我是唯一的");

    private String name;
    private String code;

    EnumSingleton(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public  String getName(){
        return name;
    }

    public  String getCode(){
        return code;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setCode(String code){
        this.code = code;
    }

}
