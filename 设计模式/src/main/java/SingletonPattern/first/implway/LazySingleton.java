package SingletonPattern.first.implway;

/**
 * 懒汉式实现方式
 * 这种实现方式在多线程的情况下并不安全
 * 普通单例类，应用场景：
 * 注册表设置，连接池，线程池等等。利用单件模式对象，我们可以确保程序中使用的全局资源只有一份
 */
public class LazySingleton {


    private static LazySingleton instance;

    private LazySingleton(){

    }

    /**
     * 延迟实例化
     * @return
     */
    public static LazySingleton getInstance(){
        if(instance==null){
            instance = new LazySingleton();
        }
        return instance;
    }

    public void sayHello(){
        System.out.println("Hello world!!");
    }


}
