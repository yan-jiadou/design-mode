package SingletonPattern.first.implway;

import java.io.Serializable;

/**
 * 可序列化的单例类，用于分布式系统的单例类
 */
public class SerializedSingleton implements Serializable {

    private static final long serialVersionUID = -78834253243544235L;

    private SerializedSingleton(){}

    private static class SingletonHelper{
        private static final SerializedSingleton instance  = new SerializedSingleton();
    }

    public static SerializedSingleton getInstance(){
        return SingletonHelper.instance;
    }

    protected Object readResolve() {
        return getInstance();
    }

}
