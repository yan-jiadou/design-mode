package SingletonPattern.first.implway;

/**
 *
 * 线程安全的懒加载单例类的实现,采用方法同步的方式或双重检测
 */
public class LazySingletonThreadSafe {

    private static LazySingletonThreadSafe instance;

    private LazySingletonThreadSafe(){}

    /**
     * 方法同步
     * @return
     */
    public static synchronized LazySingletonThreadSafe getInstance(){
        if(instance==null){
            instance = new LazySingletonThreadSafe();
        }
        return instance;
    }

    /**
     * 双重检测锁实现高性能线程安全
     * @return
     */
    public static LazySingletonThreadSafe getInstanceUsingDoubleLocking(){
        if(instance==null){
            // 通过sync锁住创建实例的代码块
            synchronized (LazySingletonThreadSafe.class){
                if(instance==null){
                    instance = new LazySingletonThreadSafe();
                }
            }
        }
        return instance;
    }

}
