package SingletonPattern.first.implway;

/**
 * 静态内部类的方式实现单例类,
 * 此方式既做到了懒加载，又实现了线程安全
 *
 * 一个类的静态内部类只有在初次使用时进行类加载
 */
public class BillPughSingleton {

    private BillPughSingleton(){}

    private static class SingletonHelper{
        private static final BillPughSingleton INSTANCE = new BillPughSingleton();
    }

    public static BillPughSingleton getInstance(){
        return SingletonHelper.INSTANCE;
    }

}
