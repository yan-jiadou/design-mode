package SingletonPattern.first.implway;

/**
 * 静态代码块方式实现单例类，此种方式和和饿汉式效果一样，都是在类加载的时候进行实例化
 */
public class StaticBlockSingleton {

    private static final StaticBlockSingleton instance;

    private StaticBlockSingleton(){}

    //static block initialization for exception handling
    static{
        try{
            instance = new StaticBlockSingleton();
        }catch(Exception e){
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    public static StaticBlockSingleton getInstance(){
        return instance;
    }
}
