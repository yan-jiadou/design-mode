package SingletonPattern.second;

public class ChocolateBoiler {

    private boolean empty;
    private boolean boiled;

    //最开始，锅炉是空的，炉内为煮沸的
    public ChocolateBoiler(){
        empty = true;
        boiled = false;
    }

    public boolean isEmpty(){
        return empty;
    }
    public boolean isBoiled(){
        return boiled;
    }

    /**
     * 锅炉填满了原料
     */
    public void fill(){
        if(isEmpty()){
            empty = false;
            boiled = false;
        }
    }

    public void drain(){
        if(!isEmpty()&&isBoiled()){
            empty = true;
        }
    }
    public void boil(){
        if(!isEmpty()&&!isBoiled()){
           boiled = true;
        }
    }
}
