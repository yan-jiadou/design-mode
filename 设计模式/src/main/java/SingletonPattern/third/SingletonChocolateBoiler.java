package SingletonPattern.third;

/**
 * 单例模式确保了一个类只有一个实例，并提供了一个全局访问点
 */
public class SingletonChocolateBoiler {

    private boolean empty;
    private boolean boiled;
    public static SingletonChocolateBoiler Instance;

    /**
     * 通过私有化构造方法，达到单一实例的效果
     */
    private SingletonChocolateBoiler(){
        empty = true;
        boiled = false;
    }

    /**
     * 获取实例唯一入口
     * @return 此类的唯一实例
     */
    public static SingletonChocolateBoiler getInstance(){
        if(Instance==null){
            Instance = new SingletonChocolateBoiler();
            System.out.println("构造了一个新SingletonChocolateBoiler对象");
        }
        return Instance;
    }

    public boolean isEmpty(){
        return empty;
    }
    public boolean isBoiled(){
        return boiled;
    }

    /**
     * 锅炉填满了原料
     */
    public void fill(){
        if(isEmpty()){
            empty = false;
            boiled = false;
        }
    }

    public void drain(){
        if(!isEmpty()&&isBoiled()){
            empty = true;
        }
    }
    public void boil(){
        if(!isEmpty()&&!isBoiled()){
            boiled = true;
        }
    }
}
