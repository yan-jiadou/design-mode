package SingletonPattern.third;

public class FirstSafeSingleton {
    private boolean empty;
    private boolean boiled;
    private static FirstSafeSingleton Instance;

    private FirstSafeSingleton(){
        empty = true;
        boiled = false;
    }

    /**
     * 通过这样进行同步确实可以解决问题，但是通过同步加锁是一定会增加性能开销的
     * 因为我们只需要在第一次构造实例时进行同步即可，其他的情况不需要同步
     * @return
     */
    public static synchronized FirstSafeSingleton getInstance(){
        if(Instance == null){
            Instance = new FirstSafeSingleton();
            System.out.println("构造一个新的FirstSafeSingleton");
        }
        return Instance;
    }

    public boolean isEmpty(){
        return empty;
    }
    public boolean isBoiled(){
        return boiled;
    }

    /**
     * 锅炉填满了原料
     */
    public void fill(){
        if(isEmpty()){
            empty = false;
            boiled = false;
        }
    }

    public void drain(){
        if(!isEmpty()&&isBoiled()){
            empty = true;
        }
    }
    public void boil(){
        if(!isEmpty()&&!isBoiled()){
            boiled = true;
        }
    }
}
