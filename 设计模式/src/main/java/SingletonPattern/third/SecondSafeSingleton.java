package SingletonPattern.third;

public class SecondSafeSingleton {
    private boolean empty;
    private boolean boiled;
    private volatile static SecondSafeSingleton Instance;

    private SecondSafeSingleton(){
        empty = true;
        boiled = false;
    }

    /**
     * 双重检查锁机制解决first中引起的性能降低
     * @return
     */
    public static SecondSafeSingleton getInstance(){
        if(Instance==null){
            synchronized (SecondSafeSingleton.class){
                if(Instance==null){
                    Instance = new SecondSafeSingleton();
                    System.out.println("构造了一个新的SecondSafeSingleton");
                }
            }
        }
        return Instance;
    }


    public boolean isEmpty(){
        return empty;
    }
    public boolean isBoiled(){
        return boiled;
    }

    /**
     * 锅炉填满了原料
     */
    public void fill(){
        if(isEmpty()){
            empty = false;
            boiled = false;
        }
    }

    public void drain(){
        if(!isEmpty()&&isBoiled()){
            empty = true;
        }
    }
    public void boil(){
        if(!isEmpty()&&!isBoiled()){
            boiled = true;
        }
    }
}
