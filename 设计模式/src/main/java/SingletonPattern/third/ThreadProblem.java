package SingletonPattern.third;

/**
 * 按道理说，使用Singleton对象时，无论多少个线程使用它，它都应该始终构造一次即可，但是，
 * 如果按照second中的做法去设计类，时会出现线程安全问题的，例如如下
 */
public class ThreadProblem {
    public static void main(String[] args) {

        // 这个存在线程安全的问题
        for(int i=0;i<1000;i++){
            new Thread(()->{
                SingletonChocolateBoiler.getInstance();
            }).start();
        }

        //通过synchronized解决后测试
        for(int i=0;i<1000;i++){
            new Thread(()->{
                FirstSafeSingleton.getInstance();
            }).start();
        }

        //通过双重加锁机制解决
        for(int i=0;i<1000;i++){
            new Thread(()->{
                SecondSafeSingleton.getInstance();
            }).start();
        }
    }
}
