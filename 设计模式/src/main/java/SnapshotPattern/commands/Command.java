package SnapshotPattern.commands;

public interface Command {
    String getName();
    void execute();
}
