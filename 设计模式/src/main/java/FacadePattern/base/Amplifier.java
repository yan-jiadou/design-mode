package FacadePattern.base;

/**
 * 扬声器
 */
public class Amplifier {

    public void on(){
        System.out.println("打开扬声器");
    }

    public void off(){
        System.out.println("关闭扬声器");
    }

    void setCd(CdPlayer cd){
        cd.setAmplifier(this);
        System.out.println("设置接入Cd");

    }

   public void setDvd(DvdPlayer dvd){
        dvd.setAmplifier(this);
       System.out.println("设置接入DVD");
    }


    public void setSurroundSound(){
        System.out.println("开启环绕音效！！");
    }

   public  void setVolume(int i){
       System.out.println("设置音量为："+i);
    }

}
