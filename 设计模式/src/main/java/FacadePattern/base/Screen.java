package FacadePattern.base;

/**
 * 白幕布
 */
public class Screen {
    public void down() {
        System.out.println("降幕布");
    }

    public void up() {
        System.out.println("升幕布");
    }
}
