package FacadePattern.base;

/**
 * 爆米花机
 */
public class PopcornPopper {

    public void on(){
        System.out.println("爆米花机");

    }

    public void pop() {
        System.out.println("开始制作爆米花");
    }

    public void off() {
        System.out.println("关闭爆米花机");
    }
}
