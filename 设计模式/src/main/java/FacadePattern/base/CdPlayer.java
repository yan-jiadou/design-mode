package FacadePattern.base;

/**
 * CD播放器
 */
public class CdPlayer {

    Amplifier amplifier;

    public void setAmplifier(Amplifier amplifier){
        this.amplifier = amplifier;
    }

    public void play(String cdName){
        System.out.println("开始播放："+ cdName);
    }

    public void on(){
        System.out.println("打开CDPlayer");
        amplifier.on();
    }

    public void off(){
        System.out.println("关闭CDPlayer");
        amplifier.off();
    }
}
