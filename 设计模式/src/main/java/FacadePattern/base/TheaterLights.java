package FacadePattern.base;

/**
 * 剧院灯
 */
public class TheaterLights {
    public void dim(int i) {
        System.out.println("调节亮度为"+i);
    }

    public void on() {
        System.out.println("打开影院灯光效果");
    }
}
