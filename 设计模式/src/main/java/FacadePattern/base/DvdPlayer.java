package FacadePattern.base;

/**
 * DvD播放器
 */
public class DvdPlayer {

    Amplifier amplifier;

    public void setAmplifier(Amplifier amplifier){
        this.amplifier = amplifier;
    }

    public void on() {
        System.out.println("打开DVD");
        amplifier.on();
    }

    public void play(String movie) {
        System.out.println("playing"+movie);
    }

    public void stop() {
        System.out.println("停止播放");
    }

    public void eject() {
    }

    public void off() {
        System.out.println("关闭DVD");
        amplifier.off();
    }
}
