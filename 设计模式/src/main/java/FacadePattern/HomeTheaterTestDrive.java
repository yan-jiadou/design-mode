package FacadePattern;

import FacadePattern.application.HomeTheaterFacade;

/**
 * 外观模式提供了一个统一的接口，用来访问子系统中的一群接口。
 * 外观定义了一个高层接口，让子系统更容易被使用。
 */
public class HomeTheaterTestDrive {
    public static void main(String[] args) {
        HomeTheaterFacade homeTheaterFacade = new HomeTheaterFacade();
        homeTheaterFacade.watchMovie("倚天屠龙记");
        homeTheaterFacade.endMovie();
    }
}
