package StrategyPattern.appcalition.fly;

import StrategyPattern.base.FlyBehavior;

public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("我飞不起来！-！");
    }
}
