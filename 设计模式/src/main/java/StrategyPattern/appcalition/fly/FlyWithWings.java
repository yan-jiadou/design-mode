package StrategyPattern.appcalition.fly;

import StrategyPattern.base.FlyBehavior;

public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("我飞起来了！！");
    }
}
