package StrategyPattern.appcalition;

import StrategyPattern.base.Duck;
import StrategyPattern.base.FlyBehavior;
import StrategyPattern.base.QuackBehavior;

public class MallardDuck extends Duck {
    @Override
    public void disPlay() {
        System.out.println("I am a MallardDuck!1");
    }
    public MallardDuck(){
    }
    public MallardDuck(QuackBehavior quackBehavior, FlyBehavior flyBehavior){
        setQuackBehavior(quackBehavior);
        setFlyBehavior(flyBehavior);
    }
}
