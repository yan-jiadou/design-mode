package StrategyPattern.appcalition.quack;

import StrategyPattern.base.QuackBehavior;

public class MuteQuack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println(".....<Silence>.....");
    }
}
