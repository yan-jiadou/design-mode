package StrategyPattern.appcalition.quack;

import StrategyPattern.base.QuackBehavior;

public class Quack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("呱呱呱！");
    }
}
