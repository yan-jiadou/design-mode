package StrategyPattern.appcalition.quack;

import StrategyPattern.base.QuackBehavior;

public class Squeak implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Squeak");
    }
}
