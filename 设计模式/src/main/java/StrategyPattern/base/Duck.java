package StrategyPattern.base;

/**
 * 鸭子抽象类，组合了飞行接口和呱呱接口，
 * 让鸭子的行为变为动态可变的属性
 * 实现了面对抽象编程
 */
public abstract class Duck {

    //接口，实现可变
    FlyBehavior flyBehavior;
    //接口，实现可变
    QuackBehavior quackBehavior;

    public Duck(){
    }

    public abstract void disPlay();

    public void performFly(){
        flyBehavior.fly();
    }

    public void performQuack(){
        quackBehavior.quack();
    }

    public void swim(){
        System.out.println("All ducks float , even decoys!!");
    }

    public void setFlyBehavior(FlyBehavior fb){
        this.flyBehavior = fb;
    }

    public void setQuackBehavior(QuackBehavior qb){
        this.quackBehavior = qb;
    }


}
