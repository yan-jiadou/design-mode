package StrategyPattern.base;

/**
 * 将飞行行为抽象为接口
 */
public interface FlyBehavior {
    void fly();
}
