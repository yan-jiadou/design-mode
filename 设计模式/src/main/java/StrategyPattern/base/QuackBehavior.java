package StrategyPattern.base;

/**
 * 将叫的行为抽象为接口
 */
public interface QuackBehavior {
    void quack();
}
