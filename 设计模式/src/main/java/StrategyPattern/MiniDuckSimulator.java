package StrategyPattern;

import StrategyPattern.appcalition.MallardDuck;
import StrategyPattern.appcalition.fly.FlyNoWay;
import StrategyPattern.appcalition.fly.FlyWithWings;
import StrategyPattern.appcalition.quack.MuteQuack;
import StrategyPattern.appcalition.quack.Quack;
import StrategyPattern.base.Duck;

/**
 * 策略模式：定义算法族(抽象接口)，分别封装起来(不同实现)，让他们之间可以相互替换(接口引用组合到客户实例中)
 * 此模式让算法的变化独立于使用算法的客户。
 */
public class MiniDuckSimulator {
    public static void main(String[] args) {
        Duck mallard = new MallardDuck(new Quack(),new FlyWithWings());
        mallard.performQuack();
        mallard.performFly();
        Duck duck2 = new MallardDuck(new MuteQuack(),new FlyNoWay());
        duck2.performQuack();
        duck2.performFly();
    }
}
