package IteratorPattern.base;

public interface Iterator {
    boolean hasNext();
    Object next();
}
