package IteratorPattern.first;

import java.util.List;

public class FirstTest {
    public static void main(String[] args) {
        //针对DinerMenu的用法
        DinerMenu dinerMenu = new DinerMenu();
        MenuItem[] menuItems = dinerMenu.getMenuItems();
        for(int i=0;i<dinerMenu.numberOfItems;i++){
            System.out.println(menuItems[i].getName());
        }

        //针对PancakeHouseMenu的用法
        PancakeHouseMenu pancakeHouseMenu = new PancakeHouseMenu();
        List list = pancakeHouseMenu.getMenuItems();
        for(Object obj:list){
            MenuItem menuItem = (MenuItem) obj;
            System.out.println(menuItem.getName());
        }

    }
}
