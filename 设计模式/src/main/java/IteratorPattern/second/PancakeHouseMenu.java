package IteratorPattern.second;

import IteratorPattern.base.Iterator;
import IteratorPattern.first.MenuItem;

import java.util.ArrayList;

/**
 * 煎饼屋菜单
 */
public class PancakeHouseMenu {

    ArrayList menuItems;

    public PancakeHouseMenu(){

       menuItems = new ArrayList();

       addItem("K&B's Pancake Breakfast","Pancakes with scrambled eggs,and toast",true,2.99);

       addItem("Regular Pancake Breakfast","Pancakes with fried eggs, sausage",false,3.49);

       addItem("Blueberry Pancakes","Pancakes made with fresh blueberries",true,2.99);

       addItem("Waffles","Waffles ,with your choice of blueberries or strawberries",true,3.59);

    }

    public void addItem(String name, String description,boolean vegetarian,double price){
        MenuItem menuItem = new MenuItem(name,description,vegetarian,price);
        menuItems.add(menuItem);
    }

    public ArrayList getMenuItems(){
        return menuItems;
    }

    public Iterator createIterator(){
        return new PancakeHouseMenuIterator(menuItems);
    }

    class PancakeHouseMenuIterator implements Iterator {

        ArrayList menuItems;
        int position = 0;

        public PancakeHouseMenuIterator(ArrayList menuItems){
            this.menuItems = menuItems;
        }

        @Override
        public boolean hasNext() {
            if(position>=menuItems.size()||menuItems.get(position)==null)
                return false;
            else return true;
        }

        @Override
        public Object next() {
            MenuItem menuItem = (MenuItem) menuItems.get(position);
            position+=1;
            return menuItem;
        }
    }

}
