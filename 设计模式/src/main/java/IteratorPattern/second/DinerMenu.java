package IteratorPattern.second;

import IteratorPattern.base.Iterator;
import IteratorPattern.first.MenuItem;

public class DinerMenu {
    static final int MAX_ITEMS = 6;
    public int numberOfItems = 0;
    MenuItem[] menuItems;

    public DinerMenu(){
        menuItems = new MenuItem[MAX_ITEMS];

        addItem("Vegetarian BLT","(Fakin) Bacon with lettuce & tomato on whole wheat",true,2.99);

        addItem("BLT","Bacon with lettuce & tomato on whole wheat",false,2.99);

        addItem("Hotdog","A hot dog , with saurkraut,relish,onions , topped with cheese",false,3.05);

    }

    public void addItem(String name,String description,boolean vegetarian,double price){
        MenuItem menuItem = new MenuItem(name,description,vegetarian,price);
        if(numberOfItems >= MAX_ITEMS) {
            System.out.println("Sorry, menu is null! Can't add item to menu");
        }else{
            menuItems[numberOfItems] = menuItem;
            numberOfItems = numberOfItems + 1;
        }
    }

    public MenuItem[] getMenuItems(){
        return menuItems;
    }

    public Iterator createIterator(){
        return new DinerMenuIterator(menuItems);
    }


    class DinerMenuIterator implements Iterator {
        MenuItem[] items;
        int position = 0;

        public DinerMenuIterator(MenuItem[] items){
            this.items = items;
        }

        @Override
        public boolean hasNext() {
            if(position>=items.length||items[position]==null){
                return false;
            }else {
                return true;
            }
        }

        @Override
        public Object next() {
            MenuItem menuItem = items[position];
            position+=1;
            return menuItem;
        }

    }

}
