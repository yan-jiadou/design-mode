package IteratorPattern.second;

import IteratorPattern.base.Iterator;
import IteratorPattern.first.MenuItem;

public class Alice {

    /**
     * 通过迭代器完成遍历
     */
    public void printMenuByIterator(){
        PancakeHouseMenu pancakeHouseMenu = new PancakeHouseMenu();
        Iterator pancakeIterator = pancakeHouseMenu.createIterator();
        DinerMenu dinerMenu = new DinerMenu();
        Iterator dinerIterator = dinerMenu.createIterator();
        printMenu(pancakeIterator);
        printMenu(dinerIterator);
    }

    /**
     * 通过统一的迭代器方式进行遍历
     * @param iterator
     */
    private void printMenu(Iterator iterator){
        while (iterator.hasNext()){
            MenuItem item = (MenuItem) iterator.next();
            System.out.println(item.getName()+" "+item.getPrice());
            System.out.println(item.getDescription());
        }
    }


}
