package DecoratorPattern.base;

/**
 * 调料抽象类
 */
public abstract class CondimentDecorator extends Beverage{
    public abstract String getDescription();
}
