package DecoratorPattern.first;

import DecoratorPattern.first.coffee.Decaf;
import DecoratorPattern.first.coffee.HouseBlend;

public class MainTest {
    public static void main(String[] args) {
        System.out.println("用户A 点一杯HouseBlend 价格为："+new HouseBlend().cost());
        System.out.println("用户B 点一杯 Decaf 价格为："+ new Decaf().cost());
    }
}
