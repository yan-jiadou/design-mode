package DecoratorPattern.first.coffee;

import DecoratorPattern.first.base.Beverage;

/**
 * DarkRoast 定价 10元
 */
public class DarkRoast extends Beverage {
    @Override
    public double cost() {
        return 10;
    }
}
