package DecoratorPattern.first.coffee;

import DecoratorPattern.first.base.Beverage;

/**
 * Decaf 定价 8元
 */
public class Decaf extends Beverage {
    @Override
    public double cost() {
        return 8;
    }
}
