package DecoratorPattern.first.coffee;

import DecoratorPattern.first.base.Beverage;

/**
 * HouseBlend 定价 5元
 */
public class HouseBlend extends Beverage {
    @Override
    public double cost() {
        return 5;
    }
}
