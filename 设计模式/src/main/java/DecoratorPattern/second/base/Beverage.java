package DecoratorPattern.second.base;

public abstract class Beverage {

    private String description;

    private boolean milk;//定价1块
    private boolean soy;//定价2块
    private boolean mocha;//定价3块
    private boolean whip;//定价4块

    public Beverage(boolean milk,boolean soy,boolean mocha,boolean whip){
        this.milk = milk;
        this.soy = soy;
        this.mocha = mocha;
        this.whip = whip;
    }

    public double cost(){
        double addMoney = 0;
        if(hasMilk()) addMoney+=1;
        if(hasMocha()) addMoney+=3;
        if(hasSoy()) addMoney+=2;
        if(hasWhip()) addMoney+=4;
        return addMoney;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMilk(boolean milk) {
        this.milk = milk;
    }

    public void setSoy(boolean soy) {
        this.soy = soy;
    }

    public void setMocha(boolean mocha) {
        this.mocha = mocha;
    }

    public void setWhip(boolean whip) {
        this.whip = whip;
    }

    public boolean hasMilk() {
        return milk;
    }

    public boolean hasSoy() {
        return soy;
    }

    public boolean hasMocha() {
        return mocha;
    }

    public boolean hasWhip() {
        return whip;
    }
}
