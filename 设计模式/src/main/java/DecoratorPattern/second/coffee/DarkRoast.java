package DecoratorPattern.second.coffee;


import DecoratorPattern.second.base.Beverage;

/**
 * DarkRoast 定价 10元
 */
public class DarkRoast extends Beverage {
    public DarkRoast(boolean milk, boolean soy, boolean mocha, boolean whip) {
        super(milk, soy, mocha, whip);
    }

    @Override
    public double cost() {
        return 10+super.cost();
    }
}
