package DecoratorPattern.second.coffee;


import DecoratorPattern.second.base.Beverage;

/**
 * HouseBlend 定价 5元
 */
public class HouseBlend extends Beverage {

    public HouseBlend(boolean milk, boolean soy, boolean mocha, boolean whip) {
        super(milk, soy, mocha, whip);
    }

    @Override
    public double cost() {
        return 5+super.cost();
    }
}
