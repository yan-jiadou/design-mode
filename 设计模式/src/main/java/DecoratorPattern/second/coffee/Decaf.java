package DecoratorPattern.second.coffee;

import DecoratorPattern.second.base.Beverage;

/**
 * Decaf 定价 8元
 */
public class Decaf extends Beverage {

    public Decaf(boolean milk,boolean soy,boolean mocha,boolean whip){
        super(milk,soy,mocha,whip);
    }

    @Override
    public double cost() {
        return 8+super.cost();
    }
}
