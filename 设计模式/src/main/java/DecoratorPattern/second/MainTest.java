package DecoratorPattern.second;

import DecoratorPattern.second.coffee.Decaf;


public class MainTest {
    public static void main(String[] args) {
        //普通的Decaf
        System.out.println(new Decaf(false,false,false,false).cost());
        //加牛奶的Decaf
        System.out.println(new Decaf(true,false,false,false).cost());
    }
}
