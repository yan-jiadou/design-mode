package DecoratorPattern;

import DecoratorPattern.application.Espresso;
import DecoratorPattern.application.Mocha;
import DecoratorPattern.application.Soy;
import DecoratorPattern.base.Beverage;

/**
 * 装饰者模式：动态的将责任附到对象上，若要扩展功能，装饰者提供了比继承更有弹性的替代方案。
 */
public class StarBuzzCoffee {
    public static void main(String[] args) {
        Beverage beverage = new Espresso();
        System.out.println(beverage.getDescription()+":"+beverage.cost());

        Beverage beverage1 = new Espresso();
        beverage1 = new Mocha(beverage1);
        beverage1 = new Soy(beverage1);
        System.out.println(beverage1.getDescription()+":"+beverage1.cost());

    }
}
