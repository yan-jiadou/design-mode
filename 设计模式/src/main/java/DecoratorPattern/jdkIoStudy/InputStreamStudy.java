package DecoratorPattern.jdkIoStudy;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import jdk.internal.util.xml.impl.Input;

import java.io.*;
import java.net.Socket;

public class InputStreamStudy {
    public static void main(String[] args) throws IOException {
        InputStream inputStream = new BufferedInputStream(new FileInputStream("FileInputStreamStudy.txt"));
        System.out.println(inputStream.read());
        System.out.println(inputStream.markSupported());
//        inputStream.mark();
//        inputStream.reset();
        inputStream.close();
    }
}
