package DecoratorPattern.application;

import DecoratorPattern.base.Beverage;

/**
 *首选咖啡实体类
 */
public class HouseBlend extends Beverage {
    @Override
    public double cost() {
        return 0.90;
    }
    public HouseBlend(){
        description = "House Blend Coffee";
    }
}
