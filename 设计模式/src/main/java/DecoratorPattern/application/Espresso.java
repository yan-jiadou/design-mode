package DecoratorPattern.application;

import DecoratorPattern.base.Beverage;

/**
 * 浓缩咖啡实体类
 */
public class Espresso extends Beverage {
    @Override
    public double cost() {
        return 1.99;
    }
    public Espresso(){
        description = "Espresso";
    }
}
