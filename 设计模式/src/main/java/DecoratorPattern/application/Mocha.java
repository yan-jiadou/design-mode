package DecoratorPattern.application;

import DecoratorPattern.base.Beverage;
import DecoratorPattern.base.CondimentDecorator;

/**
 * 装饰类： Mocha调料
 */
public class Mocha extends CondimentDecorator {
    Beverage beverage;

    public Mocha(Beverage beverage){
        this.beverage = beverage;
    }

    @Override
    public double cost() {
        return 0.20+ beverage.cost();
    }

    @Override
    public String getDescription() {
        return beverage.getDescription()+ ", Mocha";
    }
}
