package DecoratorPattern.application;

import DecoratorPattern.base.Beverage;
import DecoratorPattern.base.CondimentDecorator;

public class Soy extends CondimentDecorator {
    Beverage beverage;

    public Soy(Beverage beverage){
        this.beverage = beverage;
    }

    @Override
    public double cost() {
        return beverage.cost()+0.50;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription()+",Soy";
    }
}
