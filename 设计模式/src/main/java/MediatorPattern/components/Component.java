package MediatorPattern.components;

import MediatorPattern.mediator.Mediator;

public interface Component {

    void setMediator(Mediator mediator);
    String getName();
}
