package FactoryPattern.first;

public class PizzaStore {

    void prepare(String pizzaName){
        System.out.println("准备中 "+pizzaName);
        System.out.println("和面团中....");
        System.out.println("添加酱汁");
        System.out.println("添加配料：");
    }
    void bake(){
        System.out.println("准备烘烤 25 分钟...");
    }
    void cut(){
        System.out.println("烘烤完成，进行切割");
    }
    void box(){
        System.out.println("切割完毕，进行装盒");
    }

    /**
     * 封装主流程，让用户简单调用
     * @param type
     * @return
     */
    public Pizza orderPizza(String type) {
        Pizza pizza = PizzaFactory.getPizza(type);
        assert pizza != null;
        prepare(pizza.getPizzaName());
        bake();
        cut();
        box();
        return pizza;
    }
}
