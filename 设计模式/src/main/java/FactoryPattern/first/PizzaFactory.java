package FactoryPattern.first;

/**
 * 简单Pizza工厂
 */
public class PizzaFactory {

    public static final String CHEESE = "cheese";
    public static final String GREEK = "greek";

    public static Pizza getPizza(String type) {
        Pizza pizza = null;
        if(type.equals(CHEESE)) {
            pizza = new CheesePizza();
        }
        else if(type.equals(GREEK)){
            pizza = new GreekPizza();
        }
        return pizza;
    }

}
