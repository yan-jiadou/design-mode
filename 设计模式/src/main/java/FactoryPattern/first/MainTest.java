package FactoryPattern.first;

/**
 * 简单工厂模式：
 * 将创建一类对象的细节封装在一个对象中，外界只需要通过这个对象（工厂对象）根据特定的参数直接获取想要的对象即可。
 */
public class MainTest {
    public static void main(String[] args) {
        Pizza pizza = new PizzaStore().orderPizza(PizzaFactory.CHEESE);
    }
}
