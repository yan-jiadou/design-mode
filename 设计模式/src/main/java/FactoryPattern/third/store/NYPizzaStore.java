package FactoryPattern.third.store;

import FactoryPattern.third.PizzaStore;
import FactoryPattern.third.pizza.NYPizza1;
import FactoryPattern.third.pizza.NYPizza2;
import FactoryPattern.third.pizza.Pizza;

public class NYPizzaStore extends PizzaStore {

    @Override
    protected Pizza createPizza(String type) {
        Pizza pizza = null;
        if(type.equals("NY1")){
            pizza = new NYPizza1();
        }else if(type.equals("NY2")){
            pizza = new NYPizza2();
        }
        else{
            System.out.println("此披萨店不存在该披萨");
        }
        return pizza;
    }

}
