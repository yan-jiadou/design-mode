package FactoryPattern.third.store;

import FactoryPattern.third.PizzaStore;
import FactoryPattern.third.pizza.ChicagoPizza1;
import FactoryPattern.third.pizza.Pizza;

public class ChicagoPizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String type) {
        Pizza pizza = null;
        if(type.equals("ChicagoPizza1")){
            pizza = new ChicagoPizza1();
        }
        else{
            System.out.println("此披萨店不存在该披萨");
        }
        return pizza;
    }
}
