package FactoryPattern.third;

import FactoryPattern.third.store.ChicagoPizzaStore;
import FactoryPattern.third.store.NYPizzaStore;

/**
 * 抽象工厂模式:
 * 提供一个接口，用于创建相关或依赖对象的家族，而不需要明确指定具体类。 通过依赖注入来降低耦合。
 * 本实例本不符合抽象工厂模式的设计哈哈，抽象工厂模式的学习在 AbstractFactoryPattern中
 */
public class MainTest {
    public static void main(String[] args) {
        PizzaStore pizzaStore1 = new NYPizzaStore();
        pizzaStore1.orderPizza("NY1");

        PizzaStore pizzaStore2 = new ChicagoPizzaStore();
        pizzaStore2.orderPizza("ChicagoPizza1");
    }
}
