package FactoryPattern.third.pizza;

public class Pizza {
    private String pizzaName;

    public String getPizzaName() {
        return pizzaName;
    }

    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }

    public void prepare(){
        System.out.println("准备："+ pizzaName);
    }

    public void bake(){
        System.out.println("烘烤......");
    }

    public void cut(){
        System.out.println("切片.....");
    }

    public void box(){
        System.out.println("装盒.......");
    }

}
