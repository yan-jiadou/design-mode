package FactoryPattern.third.pizza;

public class ChicagoPizza1 extends Pizza {
    public ChicagoPizza1(){
        this.setPizzaName("ChicagoPizza1");
    }


    @Override
    public void bake(){
        System.out.println("ChicagoPizza1 烘烤流程变啦");
    }
}
