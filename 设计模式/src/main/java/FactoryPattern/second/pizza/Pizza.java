package FactoryPattern.second.pizza;

public class Pizza {
    private String pizzaName;

    public String getPizzaName() {
        return pizzaName;
    }

    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }
}
