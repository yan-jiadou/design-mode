package FactoryPattern.second;

import FactoryPattern.second.factory.CaliforniaPizzaFactory;
import FactoryPattern.second.factory.ChicagoPizzaFactory;
import FactoryPattern.second.factory.NYPizzaFactory;

/**
 * 工厂方法模式：
 * 定义一个创建对象的工厂接口，将其组合到工作类中，然后实现工厂接口产生不同的实现，然后用户类可以根据自己的需求自己决定使用哪一个实现，将其注入到工作类中即可。
 * 在这个Demo中：工厂接口为PizzaFactory, 工作类为PizzaStore, 工厂具体的实现为：NYPizzaFactory，ChicagoPizzaFactory，CaliforniaPizzaFactory
 */
public class MainTest {
    public static void main(String[] args) {
        //纽约分店
        PizzaStore pizzaStore1 = new PizzaStore(new NYPizzaFactory());
        pizzaStore1.orderPizza("NY1");

        //芝加哥分店
        PizzaStore pizzaStore2 = new PizzaStore(new ChicagoPizzaFactory());
        pizzaStore2.orderPizza("ChicagoPizza1");

        //加州分店
        PizzaStore pizzaStore3 = new PizzaStore(new CaliforniaPizzaFactory());
        pizzaStore3.orderPizza("California1");
    }
}
