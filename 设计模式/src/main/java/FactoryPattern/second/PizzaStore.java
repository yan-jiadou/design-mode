package FactoryPattern.second;

import FactoryPattern.second.factory.PizzaFactory;
import FactoryPattern.second.pizza.Pizza;

public class PizzaStore {

    private PizzaFactory pizzaFactory;

    public PizzaStore(PizzaFactory pizzaFactory){
        this.pizzaFactory = pizzaFactory;
    }

    void prepare(String pizzaName){
        System.out.println("准备中 "+pizzaName);
        System.out.println("和面团中....");
        System.out.println("添加酱汁");
        System.out.println("添加配料：");
    }

    void bake(){
        System.out.println("准备烘烤 25 分钟...");
    }
    void cut(){
        System.out.println("烘烤完成，进行切割");
    }
    void box(){
        System.out.println("切割完毕，进行装盒");
    }

    public Pizza orderPizza(String type){
        Pizza pizza = pizzaFactory.getPizza(type);
        assert pizza != null;
        prepare(pizza.getPizzaName());
        bake();
        cut();
        box();
        return pizza;
    }
}
