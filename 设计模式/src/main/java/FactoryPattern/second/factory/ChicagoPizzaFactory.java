package FactoryPattern.second.factory;

import FactoryPattern.second.pizza.ChicagoPizza1;
import FactoryPattern.second.pizza.Pizza;

public class ChicagoPizzaFactory implements PizzaFactory {
    @Override
    public Pizza getPizza(String type) {
        Pizza pizza = null;
        if(type.equals("ChicagoPizza1")){
            pizza = new ChicagoPizza1();
        }
        return pizza;
    }

}
