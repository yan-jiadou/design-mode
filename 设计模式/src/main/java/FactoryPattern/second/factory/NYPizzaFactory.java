package FactoryPattern.second.factory;

import FactoryPattern.second.pizza.NYPizza1;
import FactoryPattern.second.pizza.NYPizza2;
import FactoryPattern.second.pizza.Pizza;

public class NYPizzaFactory implements PizzaFactory{
    @Override
    public Pizza getPizza(String type) {
        Pizza pizza = null;
        if(type.equals("NY1")){
            pizza = new NYPizza1();
        }else if(type.equals("NY2")){
            pizza = new NYPizza2();
        }
        return pizza;
    }
}
