package FactoryPattern.second.factory;

import FactoryPattern.second.pizza.CaliforniaPizza1;
import FactoryPattern.second.pizza.Pizza;

public class CaliforniaPizzaFactory implements PizzaFactory{
    @Override
    public Pizza getPizza(String type) {
        Pizza pizza = null;
        if(type.equals("California1")){
            pizza = new CaliforniaPizza1();
        }
        return pizza;
    }
}
