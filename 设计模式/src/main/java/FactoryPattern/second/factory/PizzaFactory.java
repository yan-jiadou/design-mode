package FactoryPattern.second.factory;

import FactoryPattern.second.pizza.Pizza;

public interface PizzaFactory {
    Pizza getPizza(String type);
}
