package AdapterPattern.base;

/**
 * 鸭子适配器，将火鸡适配成鸭子
 */
public class TurkeyToDuckAdapter implements Duck{
    private Turkey turkey;

    public TurkeyToDuckAdapter(Turkey turkey){
       this.turkey = turkey;
    }


    @Override
    public void quack() throws InterruptedException {
        for(int i=0;i<2;i++)
           turkey.gobble();
    }

    @Override
    public void fly() throws InterruptedException {
        for(int i=0;i<5;i++)
            turkey.fly();
    }
}
