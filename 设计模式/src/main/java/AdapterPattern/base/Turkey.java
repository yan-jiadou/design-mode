package AdapterPattern.base;

public interface Turkey {
    void gobble() throws InterruptedException;
    void fly() throws InterruptedException;
}
