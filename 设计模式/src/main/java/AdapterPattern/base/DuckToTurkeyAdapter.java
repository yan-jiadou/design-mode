package AdapterPattern.base;

public class DuckToTurkeyAdapter implements Turkey{
    private Duck duck;

    public DuckToTurkeyAdapter(Duck duck){
        this.duck = duck;
    }

    @Override
    public void gobble() throws InterruptedException {
        System.out.println("Quack");
    }

    @Override
    public void fly() throws InterruptedException {
        System.out.println("I am flying");
    }
}
