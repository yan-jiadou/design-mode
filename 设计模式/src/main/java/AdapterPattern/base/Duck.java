package AdapterPattern.base;

public interface Duck {
    void quack() throws InterruptedException;
    void fly() throws InterruptedException;
}
