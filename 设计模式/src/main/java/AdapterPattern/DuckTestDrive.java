package AdapterPattern;

import AdapterPattern.application.MallardDuck;
import AdapterPattern.application.WildTurkey;
import AdapterPattern.base.Duck;
import AdapterPattern.base.DuckToTurkeyAdapter;
import AdapterPattern.base.Turkey;
import AdapterPattern.base.TurkeyToDuckAdapter;

/**
 * 适配器模式将一个类的接口，转换成客户期望的另一个接口。
 * 适配器让原本的接口不兼容的类可以合作无间。
 */
public class DuckTestDrive {
    public static void main(String[] args) throws InterruptedException {
        Duck duck = new MallardDuck();
        Turkey turkey = new WildTurkey();
        Duck turkeyAdapter = new TurkeyToDuckAdapter(turkey);
        Turkey duckAdapter = new DuckToTurkeyAdapter(duck);
        testDuck(duck);
        testTurkey(turkey);
        testDuck(turkeyAdapter);
        testTurkey(duckAdapter);
    }

    static void testDuck(Duck duck) throws InterruptedException {
        duck.quack();
        duck.fly();
    }

    static void testTurkey(Turkey turkey) throws InterruptedException {
        turkey.gobble();
        turkey.fly();
    }
}
