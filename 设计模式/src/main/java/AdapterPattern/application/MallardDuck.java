package AdapterPattern.application;

import AdapterPattern.base.Duck;

public class MallardDuck implements Duck {
    @Override
    public void quack() throws InterruptedException {
        for(int i=0;i<2;i++)
          System.out.println("Quack");
    }

    @Override
    public void fly() throws InterruptedException {
        for(int i=0;i<5;i++)
           System.out.println("I am flying");
    }
}
