package AdapterPattern.application;

import AdapterPattern.base.Turkey;

public class WildTurkey implements Turkey {
    @Override
    public void gobble() throws InterruptedException {
        System.out.println("Quack");
    }

    @Override
    public void fly() throws InterruptedException {
        System.out.println("I am flying");
    }
}
