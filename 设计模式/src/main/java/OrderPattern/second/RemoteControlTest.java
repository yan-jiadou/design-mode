package OrderPattern.second;

import OrderPattern.second.application.Control.SimpleRemoteControl;
import OrderPattern.second.application.command.LightOnCommand;
import OrderPattern.second.application.electricAppliance.Light;

/**
 * 命令模式将 '请求' 封装成对象，以便使用不同的请求，队列或者日志来参数化其他对象。
 * 命令模式也支持可撤销的操作。
 */
public class RemoteControlTest {
    public static void main(String[] args) {
        //电灯实例对象
        Light light = new Light();
        //将电灯对象注入开灯命令中
        LightOnCommand lightOnCommand = new LightOnCommand(light);

        //将开灯命令对象注入控制器中
        SimpleRemoteControl remote = new SimpleRemoteControl();
        remote.setCommand(lightOnCommand);
        remote.buttonWasPressed();
    }
}
