package OrderPattern.second.application.Control;

import OrderPattern.second.base.Command;

/**
 * 远程控制器
 */
public class SimpleRemoteControl {
    //命令接口
    Command slot;

    public void setCommand(Command command){
        slot = command;
    }

    /**
     * 按钮按下了
     */
    public void buttonWasPressed(){
        slot.execute();
    }

}
