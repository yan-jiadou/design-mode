package OrderPattern.second.application.command;

import OrderPattern.second.application.electricAppliance.Light;
import OrderPattern.second.base.Command;

/**
 * 开灯命令
 */
public class LightOnCommand implements Command {

    Light light;

    public LightOnCommand(Light light){
        this.light = light;
    }
    @Override
    public void execute() {
        light.on();
    }
}
