package OrderPattern.second.application.electricAppliance;

public class Light {

   public void on(){
        System.out.println("电灯打开！");
    }

    public void off(){
        System.out.println("电灯关闭！");
    }

}
