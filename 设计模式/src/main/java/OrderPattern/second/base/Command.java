package OrderPattern.second.base;

public interface Command {
    /**
     * 命令执行方法
     */
    void execute();
}
