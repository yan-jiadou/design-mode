package OrderPattern.first;

import OrderPattern.first.fuction.*;
import OrderPattern.second.application.electricAppliance.Light;

import java.util.Stack;

/**
 * 自动化遥控器
 */
public class PrototypeControl {

    //记录操作序列实现undo
    Stack<Integer> stack = new Stack<>();
    private Object[] slotList = new Object[7];

    public void setSlot1(Object obj){
        slotList[0] = obj;
    }
    public void setSlot2(Object obj){
        slotList[1] = obj;
    }
    public void setSlot3(Object obj){
        slotList[2] = obj;
    }
    public void setSlot4(Object obj){
        slotList[3] = obj;
    }
    public void setSlot5(Object obj){
        slotList[4] = obj;
    }
    public void setSlot6(Object obj){
        slotList[5] = obj;
    }
    public void setSlot7(Object obj){
        slotList[6] = obj;
    }

    public void slot1On(boolean isUndo){
        Object obj = slotList[0];
        pressButton(obj,true);
        if(!isUndo)
        stack.push(11);
    }


    public void slot1Off(boolean isUndo){
        Object obj = slotList[0];
        pressButton(obj,false);
        if(!isUndo)
        stack.push(10);
    }


    public void slot2On(boolean isUndo){
        Object obj = slotList[1];
        pressButton(obj,true);
        if(!isUndo)
        stack.push(21);
    }


    public void slot2Off(boolean isUndo){
        Object obj = slotList[1];
        pressButton(obj,false);
        if(!isUndo)
           stack.push(20);
    }


    public void slot3On(boolean isUndo){
        Object obj = slotList[2];
        pressButton(obj,true);
        if(!isUndo)
        stack.push(31);
    }


    public void slot3Off(boolean isUndo){
        Object obj = slotList[2];
        pressButton(obj,false);
        if(!isUndo)
        stack.push(30);
    }


    public void slot4On(boolean isUndo){
        Object obj = slotList[3];
        pressButton(obj,true);
        if(!isUndo)
        stack.push(41);
    }

    public void slot4Off(boolean isUndo){
        Object obj = slotList[3];
        pressButton(obj,false);
        if(!isUndo)
        stack.push(40);
    }


    public void slot5On(boolean isUndo){
        Object obj = slotList[4];
        pressButton(obj,true);
        if(!isUndo)
        stack.push(51);
    }


    public void slot5Off(boolean isUndo){
        Object obj = slotList[4];
        pressButton(obj,false);
        if(!isUndo)
        stack.push(50);
    }


    public void slot6On(boolean isUndo){
        Object obj = slotList[5];
        pressButton(obj,true);
        if(!isUndo)
        stack.push(61);
    }


    public void slot6Off(boolean isUndo){
        Object obj = slotList[5];
        pressButton(obj,false);
        if(!isUndo)
        stack.push(60);
    }


    public void slot7On(boolean isUndo){
        Object obj = slotList[6];
        pressButton(obj,true);
        if(!isUndo)
        stack.push(71);
    }

    public void slot7Off(boolean isUndo){
        Object obj = slotList[6];
        pressButton(obj,false);
        if(!isUndo)
        stack.push(70);
    }

    /**
     * 控制全局撤销指令
     */
    public void undo(){
        int order = stack.pop();
        if(order==11){
            slot1Off(true);
        }else if(order==10){
            slot1On(true);
        }else if(order==21) slot2Off(true);
        else if(order==20) slot2On(true);
        else if(order == 31) slot3Off(true);
        else if(order == 30) slot3On(true);
        else if(order == 41) slot4Off(true);
        else if(order == 40) slot4On(true);
        else if(order == 51) slot5Off(true);
        else if(order == 50) slot5On(true);
        else if(order == 61) slot6Off(true);
        else if(order == 60) slot6On(true);
        else if(order == 71) slot7Off(true);
        else if(order == 70) slot7On(true);

    }

    private void pressButton(Object obj,boolean isOn){
        Class<?> cl = obj.getClass();
        if(cl.isAssignableFrom(CeilingFan.class)){
            CeilingFan cei = (CeilingFan) obj;
            if(isOn){
                cei.low();
            }else{
                cei.off();
            }
        }else if(cl.isAssignableFrom(CeilingLight.class)){
            CeilingLight cei = (CeilingLight) obj;
            if(isOn){
                cei.on();
            }else {
                cei.off();
            }
        }else if(cl.isAssignableFrom(GardenLight.class)){
            GardenLight gardenLight = (GardenLight) obj;
            if(isOn) gardenLight.manualOn();
            else gardenLight.manualOff();
        }else if(cl.isAssignableFrom(Light.class)){
            Light light = (Light) obj;
            if (isOn) {
                light.on();
            } else {
                light.off();
            }
        }else if(cl.isAssignableFrom(OutdoorLight.class)){
            OutdoorLight outdoorLight  = (OutdoorLight) obj;
            if(isOn) outdoorLight.on();
            else outdoorLight.off();
        }else if(cl.isAssignableFrom(Sprinkler.class)){
            Sprinkler sprinkler = (Sprinkler) obj;
            if(isOn) sprinkler.waterOn();
            else sprinkler.waterOff();

        }else if(cl.isAssignableFrom(Stereo.class)){
            Stereo stereo = (Stereo) obj;
            if(isOn) stereo.on();
            else stereo.off();
        }else if(cl.isAssignableFrom(TV.class)){
            TV tv = (TV) obj;
            if(isOn) tv.on();
            else tv.off();
        }

    }


}
