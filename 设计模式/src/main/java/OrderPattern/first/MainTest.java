package OrderPattern.first;

import OrderPattern.first.fuction.CeilingFan;
import OrderPattern.first.fuction.CeilingLight;

public class MainTest {
    public static void main(String[] args) {
        //测试遥控器的开关按钮
        PrototypeControl prototypeControl  = new PrototypeControl();
        //将电器接口插入卡槽中
        prototypeControl.setSlot1(new CeilingLight());
        prototypeControl.setSlot2(new CeilingFan());
        //按下按钮实现控制
        prototypeControl.slot1On(false);
        prototypeControl.slot2On(false);
        prototypeControl.undo();
        prototypeControl.undo();
    }
}
