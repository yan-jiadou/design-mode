package OrderPattern.first.fuction;

public class CeilingLight {

    public void on(){
        System.out.println("打开吊灯");

    }

    public void off(){
        System.out.println("关闭吊灯");

    }

    public void dim(){
        System.out.println("森林模式打开");
    }

}
