package OrderPattern.first.fuction;

public class CeilingFan {

    public void high(){
        System.out.println("高风速");

    }

    public void medium(){
        System.out.println("中风速");
    }

    public void low(){
        System.out.println("低风速");
    }

    public void off(){
        System.out.println("关闭风扇");

    }

    public void getSpeed(){
        System.out.println("目前速度是：");
    }


}
