package OrderPattern.first.fuction;

public class GardenLight {

    public void setDuskTime(){
        System.out.println("设置黄昏时间模式");

    }

    public void setDawnTime(){
        System.out.println("设置落日时间模式");

    }

    public void manualOn(){
        System.out.println("手动开灯");

    }

    public void manualOff(){
        System.out.println("手动关闭");
    }
}
