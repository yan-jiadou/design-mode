package OrderPattern.first.fuction;

public class TV {

    public void on(){
        System.out.println("打开电视");
    }

    public void off(){
        System.out.println("关闭电视");

    }

    public void setInputChannel(){
        System.out.println("设置频道");

    }

    public void setVolume(){
        System.out.println("设置音量");
    }

}
