package OrderPattern.third;

public interface Command {
    /**
     * 命令执行方法
     */
    void execute();

    void undo();
}
