package OrderPattern.third.command;

import OrderPattern.third.Command;
import OrderPattern.third.appliance.Light;

public class LightOnCommand implements Command {
    Light light;

    public LightOnCommand(Light light){
        this.light = light;
    }
    @Override
    public void execute() {
        light.on();
    }

    @Override
    public void undo() {
        light.off();

    }
}
