package OrderPattern.third.command;

import OrderPattern.third.Command;

public class CeilingFanOnHighCommand implements Command {
    @Override
    public void execute() {

    }

    @Override
    public void undo() {

    }
}
