package OrderPattern.third.command;

import OrderPattern.third.Command;

public class NoCommand implements Command {
    @Override
    public void execute() {

    }

    @Override
    public void undo() {

    }
}
