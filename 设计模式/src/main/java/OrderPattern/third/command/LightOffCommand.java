package OrderPattern.third.command;

import OrderPattern.third.Command;
import OrderPattern.third.appliance.Light;

public class LightOffCommand implements Command {
    Light light;
    public LightOffCommand(Light light){
        this.light = light;
    }
    @Override
    public void execute() {
        light.off();
    }

    @Override
    public void undo() {
        light.on();

    }
}
