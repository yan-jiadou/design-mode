package OrderPattern.third.command;

import OrderPattern.third.Command;
import OrderPattern.third.appliance.CeilingFan;

public class CeilingFanOnMediumCommand implements Command {
    CeilingFan ceilingFan;

    public CeilingFanOnMediumCommand(CeilingFan fan){
        this.ceilingFan = fan;
    }

    @Override
    public void execute() {
        ceilingFan.medium();
    }

    @Override
    public void undo() {
        ceilingFan.off();

    }
}
