package OrderPattern.third.command;

import OrderPattern.third.Command;
import OrderPattern.third.appliance.CeilingLight;

public class CeilingLightOnCommand implements Command {
    CeilingLight light;

    public CeilingLightOnCommand(CeilingLight light){
        this.light  = light;
    }

    @Override
    public void execute() {
        light.on();
    }

    @Override
    public void undo() {
        light.off();

    }
}
