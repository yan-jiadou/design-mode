package OrderPattern.third.command;

import OrderPattern.third.Command;
import OrderPattern.third.appliance.CeilingFan;

public class CeilingFanOnLowCommand implements Command {

    CeilingFan ceilingFan;

    public CeilingFanOnLowCommand(CeilingFan ceilingFan){
        this.ceilingFan = ceilingFan;
    }
    @Override
    public void execute() {
        ceilingFan.low();
    }

    @Override
    public void undo() {
        ceilingFan.off();

    }
}
