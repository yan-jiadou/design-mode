package OrderPattern.third.appliance;

public class OutdoorLight {

    public void on(){
        System.out.println("门外灯打开了！！");
    }

    public void off(){
        System.out.println("门外灯关闭了!!");
    }
}
