package OrderPattern.third.appliance;

public class Stereo {

    public void on(){
        System.out.println("打开音响");
    }

    public void off(){
        System.out.println("关闭音响");
    }

    public void setCd(){
        System.out.println("放入CD");

    }

    public void setDvd(){
        System.out.println("放入DVD");

    }

    public void setRadio(){
        System.out.println("设置无线频道");

    }

    public void setVolume(){
        System.out.println("设置音量");
    }
}
