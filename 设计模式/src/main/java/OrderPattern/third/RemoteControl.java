package OrderPattern.third;

import OrderPattern.third.command.NoCommand;

/**
 * 这里的遥控器的卡槽按钮操作采用更为smart的做法
 */
public class RemoteControl {

    public static final int SLOT1 = 0;
    public static final int SLOT2 = 1;
    public static final int SLOT3 = 2;
    public static final int SLOT4 = 3;
    public static final int SLOT5 = 4;
    public static final int SLOT6 = 5;
    public static final int SLOT7 = 6;

    Command[] onCommands;
    Command[] offCommands;
    Command undoCommand;

    public RemoteControl(){
        onCommands = new Command[7];//七个卡槽
        offCommands = new Command[7];
        Command noCommand = new NoCommand();
        //七个卡槽初始化为无命令
        for(int i=0;i<7;i++){
            onCommands[i] = noCommand;
            offCommands[i] = noCommand;
        }
        undoCommand = noCommand;
    }

    public void setCommand(int slot,Command onCommand,Command offCommand) throws Exception {
        checkSlot(slot);
        onCommands[slot] = onCommand;
        offCommands[slot] = offCommand;
    }

    public void onButtonWasPressed(int slot) throws Exception {
        checkSlot(slot);
        onCommands[slot].execute();
        undoCommand = onCommands[slot];
    }

    public void offButtonWasPressed(int slot) throws Exception {
        checkSlot(slot);
        offCommands[slot].execute();
        undoCommand = offCommands[slot];
    }

    public void checkSlot(int slot) throws Exception {
        if(slot<0||slot>=onCommands.length){
            throw new Exception("无此卡槽，请检查！！");
        }
    }

    //全局撤销功能
    public void undoButtonWasPressed(){
        undoCommand.undo();
    }

}
