package OrderPattern.third;

import OrderPattern.third.appliance.CeilingFan;
import OrderPattern.third.appliance.Light;
import OrderPattern.third.command.CeilingFanOffCommand;
import OrderPattern.third.command.CeilingFanOnLowCommand;
import OrderPattern.third.command.LightOffCommand;
import OrderPattern.third.command.LightOnCommand;

public class MainTest {
    public static void main(String[] args) throws Exception {
        RemoteControl remoteControl = new RemoteControl();
        CeilingFan ceilingFan = new CeilingFan();
        Command CeilingFanOn = new CeilingFanOnLowCommand(ceilingFan);
        Command CeilingFanOff = new CeilingFanOffCommand(ceilingFan);
        remoteControl.setCommand(RemoteControl.SLOT1,CeilingFanOn,CeilingFanOff);
        Light light = new Light();
        Command LightOn = new LightOnCommand(light);
        Command LightOff = new LightOffCommand(light);
        remoteControl.setCommand(RemoteControl.SLOT2,LightOn,LightOff);
        remoteControl.onButtonWasPressed(RemoteControl.SLOT1);
        remoteControl.onButtonWasPressed(RemoteControl.SLOT2);
        remoteControl.undoButtonWasPressed();
    }
}
