package ProxyPattern.second;

public class UserServer extends User{
    public static void main(String[] args) {
        UserServer server = new UserServer();
        server.setAge(18);
        //模拟rmi生成的skeleton远程代理对象
        User_skeleton user_Skeleton = new User_skeleton(server);
        user_Skeleton.start();
    }
}
