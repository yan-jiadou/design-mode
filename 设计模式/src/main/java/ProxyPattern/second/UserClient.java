package ProxyPattern.second;

public class UserClient {
    public static void main(String[] args) {
        User user = new User_Stub();
        int age = user.getAge();
        System.out.println(age);
    }
}
