package ProxyPattern.second;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class User_skeleton extends Thread{

    private User userServer;

    public User_skeleton(User userServer) {
        this.userServer = userServer;

    }

    public void run() {
        ServerSocket serverSocket = null;
        ObjectInputStream read = null;
        ObjectOutputStream oos = null;
        Socket socket=null;
        try {
            serverSocket = new ServerSocket(8888);
            socket = serverSocket.accept();
            while (socket != null) {
                read = new ObjectInputStream(socket.getInputStream());
                String method = (String) read.readObject();
                if (method.equals("age")) {
                    int age = userServer.getAge();
                    oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeInt(age);
                    oos.flush();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (serverSocket != null) {
                try {
                    assert oos != null;
                    oos.close();
                    read.close();
                    socket.close();
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



}
