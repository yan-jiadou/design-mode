package ProxyPattern.dynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 代理处理器,使用它来完成代理类的定义
 */
public class ProxyHandler implements InvocationHandler {

    private final Object object;

    public ProxyHandler(Object object){
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        method.invoke(object,args);
        if(method.getName().equals("sayHello")){
            System.out.println("哈喽啊：Hello!!");
        }
        if (method.getName().equals("howAreYou")){
            System.out.println("你吃了嘛：how are you!");
        }
        return proxy;
    }

}
