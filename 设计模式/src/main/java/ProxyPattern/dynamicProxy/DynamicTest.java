package ProxyPattern.dynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * 动态代理Demo演示
 */
public class DynamicTest {
    public static void main(String[] args) {
        // 新建指定对象
        Person Chinese = new ChinesePerson();
        // 新建代理控制器
        InvocationHandler translatorsHandler = new ProxyHandler(Chinese);
        // 动态生成代理类
        Person translators = (Person) Proxy.newProxyInstance(Chinese.getClass().getClassLoader(),Chinese.getClass().getInterfaces(),translatorsHandler);

        translators.sayHello();
        translators.howAreYou();
    }
}
