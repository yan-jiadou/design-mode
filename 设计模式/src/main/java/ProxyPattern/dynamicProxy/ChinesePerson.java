package ProxyPattern.dynamicProxy;

public class ChinesePerson implements Person {

    public void sayHello(){
        System.out.println("哈喽啊！！");
    }

    public void howAreYou(){
        System.out.println("你吃饭了吗？");
    }

}
