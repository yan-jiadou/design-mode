package ProxyPattern.dynamicProxy;

public interface Person {

    public void sayHello();

    public void howAreYou();
}
