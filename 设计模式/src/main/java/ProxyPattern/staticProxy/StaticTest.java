package ProxyPattern.staticProxy;

/**
 * 静态代理：代理模式为另一个对象提供一个指定对象的替身(代理对象)以控制指定对象的访问
 */
public class StaticTest {

    public static void main(String[] args) {
        Person person = new Translators();
        person.howAreYou();
        person.sayHello();

    }
}
