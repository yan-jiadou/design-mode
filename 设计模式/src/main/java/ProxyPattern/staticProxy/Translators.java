package ProxyPattern.staticProxy;

/**
 * 英文翻译者类，它是对象ChinesePerson的代理类
 * 他和其代理的类实现了相同的接口,用来保持代理类的签名
 */
public class Translators implements Person {

    Person ChinesePerson = new ChinesePerson();

    public void sayHello(){
        ChinesePerson.sayHello();
        System.out.println("哈喽啊 : Hello!!");
    }

    public void howAreYou(){
        ChinesePerson.howAreYou();
        System.out.println("你吃了吗 ：how are you");
    }

}
