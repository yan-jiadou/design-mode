package ProxyPattern.staticProxy;

public interface Person {

     void sayHello();

     void howAreYou();
}
