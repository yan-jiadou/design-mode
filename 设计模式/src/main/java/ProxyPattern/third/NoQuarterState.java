package ProxyPattern.third;

public class NoQuarterState implements State {

    transient GumballMachine gumballMachine;

    public NoQuarterState(GumballMachine gumballMachine){
        this.gumballMachine = gumballMachine;
    }


    @Override
    public void insertQuarter() {
        System.out.println("你投入了一枚硬币");
        gumballMachine.setState(gumballMachine.hasQuarterState);
    }

    @Override
    public void ejectQuarter() {
        System.out.println("你还没有投入一枚硬币呢！！");

    }

    @Override
    public void turnCrank() {
        System.out.println("你还没有投入硬币呢！！");

    }

    @Override
    public void dispense() {
        System.out.println("你需要先投入硬币");
    }
}
