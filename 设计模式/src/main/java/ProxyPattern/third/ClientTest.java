package ProxyPattern.third;

import java.rmi.Naming;
import java.rmi.RemoteException;

/**
 *  状态模式允许对象在内部状态改变时改变它的行为，对象看起来好像修改了它的类
 */
public class ClientTest {
    public static void main(String[] args) throws RemoteException {
        new ClientTest().go();
    }
        public void go(){
            try{
                GumballMachineRemote service = (GumballMachineRemote) Naming.lookup("rmi://localhost:8888/RemoteGumball");
                System.out.println("远程调用地址结果："+service.getLocation());
                System.out.println("远程调用库存结果："+service.getCount());
            }catch(Exception e){
                e.printStackTrace();
            }
        }


}
