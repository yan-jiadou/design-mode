package ProxyPattern.third;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 定义远程接口
 */
public interface GumballMachineRemote extends Remote {

    int getCount() throws RemoteException;

    String getLocation() throws RemoteException;

    State getState() throws RemoteException;

}
