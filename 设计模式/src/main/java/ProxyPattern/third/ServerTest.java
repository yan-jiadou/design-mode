package ProxyPattern.third;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class ServerTest {
    public static void main(String[] args){
        try {
            GumballMachineRemote gumballMachineRemote = new GumballMachine(10, "南山区分机");
            LocateRegistry.createRegistry(8888);
            Naming.rebind("rmi://localhost:8888/RemoteGumball", gumballMachineRemote);
            System.out.println("Server start success!!");
        }catch (Exception e){
            e.printStackTrace();

        }
    }
}
