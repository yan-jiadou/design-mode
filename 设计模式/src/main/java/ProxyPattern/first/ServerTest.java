package ProxyPattern.first;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

public class ServerTest {
    public static void main(String[] args) {
        try{
            MyRemote server = new MyRemoteImpl();
            LocateRegistry.createRegistry(8888);
            Naming.rebind("rmi://localhost:8888/RemoteHello",server);
            System.out.println("Server start success!!");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
