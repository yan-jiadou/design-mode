package ProxyPattern.first;

import java.rmi.Naming;

public class ClientTest {
    public static void main(String[] args) {
        new ClientTest().go();
    }

    public void go(){
        try{
            MyRemote service = (MyRemote) Naming.lookup("rmi://localhost:8888/RemoteHello");
            System.out.println("远程调用结果："+service.sayHello());
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
