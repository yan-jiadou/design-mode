package TemplatePattern.first;

/**
 * 将Coffee,Tea中的方法抽象成模板类
 * 模板方法定义了一个算法的步骤，并允许子类为一个或多个步骤提供实现
 */
public abstract class CaffeineBeverage {

    public final void prepareRecipe(){
        boilWater();
        brew();
        pourInCup();
        addCondiments();
    }

    protected abstract void brew();

    protected abstract void addCondiments();

    void boilWater(){
        System.out.println("Boiling water");
    }

    void pourInCup(){
        System.out.println("Pouring into cup");
    }
}
