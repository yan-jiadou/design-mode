package TemplatePattern.second;

public class TeaWithHook extends CaffeineBeverageWithHook{
    @Override
    protected void brew() {
        System.out.println("Steeping the tea");
    }

    @Override
    protected void addCondiments() {
        System.out.println("Adding Lemon");
    }

    @Override
    public boolean brewHook(){
        return false;
    }

    @Override
    public boolean addCondimentsHook(){
        return false;
    }
    @Override
    public boolean boilWaterHook(){
        return true;
    }
    @Override
    public boolean pourInCupHook() {
        return true;
    }
}
