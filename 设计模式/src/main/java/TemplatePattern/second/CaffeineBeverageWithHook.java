package TemplatePattern.second;

/**
 * 使用钩子
 */
public abstract class CaffeineBeverageWithHook {

    public void prepareRecipe(){
        if(boilWaterHook()) boilWater();
        if(brewHook()) brew();
        if(pourInCupHook()) pourInCup();
        //这里使用钩子方法进行判断上是否执行添加调料方法
        if(addCondimentsHook()){
            addCondiments();
        }
    }

    protected abstract void brew();

    protected abstract void addCondiments();

    void boilWater(){
        System.out.println("Boiling water");
    }

    void pourInCup(){
        System.out.println("Pouring into cup");
    }

    //钩子方法，用来返回客户是否想要调料，默认返回true
    public boolean addCondimentsHook(){
        return true;
    }

    public boolean boilWaterHook(){
        return true;
    }

    public boolean brewHook(){
        return true;
    }

    public boolean pourInCupHook(){
        return true;
    }

}
