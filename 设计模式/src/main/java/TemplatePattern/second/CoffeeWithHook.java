package TemplatePattern.second;

public class CoffeeWithHook extends CaffeineBeverageWithHook{
    @Override
    protected void brew() {
        System.out.println("Dripping Coffee through filter");
    }

    @Override
    protected void addCondiments() {
        System.out.println("Adding Sugar and Milk");
    }

    @Override
    public boolean brewHook(){
        return false;
    }

    @Override
    public boolean addCondimentsHook(){

        return false;
    }
    @Override
    public boolean boilWaterHook(){
        return true;
    }
    @Override
    public boolean pourInCupHook() {
        return true;
    }
}
