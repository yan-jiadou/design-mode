package TemplatePattern.jdk;

import java.util.Arrays;

public class DuckSortTestDrive {
    public static void main(String[] args) {
        Duck[] ducks = {
          new Duck("Daffy",8),
          new Duck("Dewey",2)
        };
        Arrays.sort(ducks);
    }
}
