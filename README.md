# 设计模式

#### 介绍
Head First设计模式学习代码完整实现和UML图分享（drawio），持续更新中。<br/>
配套学习博客如下(同步更新)：
https://blog.csdn.net/c1776167012/category_11806934.html?spm=1001.2014.3001.5482<br/>

项目结构：<br/>
UML: 存放各种设计模式的UML的设计图(和博客配套) <br/>
src: 已包为单元，一个包就是对一个设计模式的完整学习，例如包 StrategyPattern里包含了对策略模式的所有学习代码（和博客配套）<br/>


目前包含：策略模式，观察者模式，装饰者模式，工厂模式，抽象工厂模式，单例模式，命令模式，适配器模式，模板方法模式，迭代器模式，组合模式，状态模式，代理模式<br/>



#### 安装教程

1.每个学习包都有执行主类，开箱即用，完全基于jdk 1.8，无其他依赖。


#### 使用说明

1.  下载代码
2.  解压
3.  用Idea社区版打开即可

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
